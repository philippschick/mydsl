import MyInterpreter.Interpreter

String stCode
String stTokenstream
String stCompilation

if(args.length == 0)
{
  println "Error: you must specify a source file!"
  return
}
stCode = new File(args[0]).text

// lexikalische Analyse
stTokenstream = Tokenizer.tokenize(stCode)

// Syntaktische Analyse
stCompilation = MyCompiler.compile(stTokenstream)

// Semantische Analyse und Ausführung
Interpreter.run(stCompilation)
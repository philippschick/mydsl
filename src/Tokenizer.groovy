import groovy.json.JsonOutput

class Tokenizer {
    private static def TOKENS = [
            '+'     : "PLUS",
            '-'     : "MINUS",
            '*'     : "TIMES",
            '/'    : "DIVIDE",
            '**'    : "POWER",
            ':='    : "ASSIGN",
            ';'     : "SEMICOLON",
            '='     : "EQUAL",
            '<>'    : "UNEQUAL",
            '<'     : "LT",
            '<='    : "LTEQ",
            '>'     : "GT",
            '>='    : "GTEQ",
            '('     : "LPAR",
            ')'     : "RPAR",
            '{'     : "LBRACE",
            '}'     : "RBRACE",
            '['     : "LBRACKET",
            ']'     : "RBRACKET",
            '&'     : "AMPERSAND",
            ','     : "COMMA",
    ]

    private final static def RESERVED_WORDS = [
            "if"        : "IF",
            "then"      : "THEN",
            "else"      : "ELSE",
            "elseif"    : "ELSEIF",
            "endif"     : "ENDIF",
            "for"       : "FOR",
            "in"        : "IN",
            "do"        : "DO",
            "enddo"     : "ENDDO",
            "write"     : "WRITE",
            "true"      : "TRUE",
            "false"     : "FALSE",
            "null"      : "NULL",
            "not"       : "NOT",
            "and"       : "AND",
            "or"        : "OR",
            "..."       : "SEQTO",
            "maximum"   : "MAXOP",
            "minimum"   : "MINOP",
            "first"     : "FIRSTOP",
            "last"      : "LASTOP",
            "sum"       : "SUMOP",
            "count"     : "COUNTOP",
            "is"        : "IS",
            "within"    : "WITHIN",
            "to"        : "TO",
            "number"    : "NUMBER",
            "text"      : "TEXT",
            "list"      : "LIST",
            "boolean"   : "BOOLEAN",
            "now"       : "NOW",
            "currenttime": "NOW",
            "time"      : "TIME",
            "of"        : "OF",
            "earliest"  : "EARLIEST",
            "read"      : "READ",
    ]

    private final static def REGEX_EXPRESSIONS = [
            /^\d{4}[-]\d{2}[-]\d{2}[T](\d{2}[:]\d{2}[:]\d{2})?/ : "TIMESTAMP",
            /^["][^"]*["]/               : "STRTOKEN",
            /^\d+/                       : "NUMTOKEN",
            /^[\/][\/][^\n]*/            : "COMMENT",
    ]

    static String tokenize(String stCode)
    {
        def     resultList  = []
        int     iLineNum    = 1
        int     iColumnNum  = 1

        TOKENS = TOKENS.sort { a, b -> b.key <=> a.key }

        while(stCode.length() > 0)
        {
            if((stCode.startsWith("\n")) || stCode.startsWith("\r\n"))
            {
                if(stCode.startsWith("\n"))
                    stCode = stCode.drop(1)
                else
                    stCode = stCode.drop(2)

                iLineNum++
                iColumnNum = 0
            }
            else if(stCode.startsWith(" ") || stCode.startsWith("\t"))
            {
                stCode = stCode.drop(1)
                iColumnNum++
            }
            else
            {
                boolean bIdentifiedToken = false

                // regex
                for(def expr in REGEX_EXPRESSIONS)
                {
                    if(stCode ==~ expr.key + ~/[\S\s]*/)
                    {
                        String stMatched

                        if(expr.value == "TIMESTAMP")
                        {
                            stMatched = (stCode =~ expr.key)[0][0]
                        }
                        else
                        {
                            stMatched = (stCode =~ expr.key)[0]
                        }
                        stCode = stCode.drop(stMatched.length())

                        if(expr.value == "STRTOKEN")
                        {
                            stMatched = stMatched.drop(1)
                            stMatched = stMatched.take(stMatched.length() - 1)
                        }

                        resultList.add([iLineNum.toString(), expr.value, stMatched])

                        iColumnNum += stMatched.length()
                        bIdentifiedToken = true
                        break
                    }
                }

                if(!bIdentifiedToken) {
                    for (def token in TOKENS) {
                        // check for operators and brackets
                        if (stCode.startsWith(token.key)) {
                            stCode = stCode.drop(token.key.length())
                            iColumnNum += token.key.length()

                            resultList.add([iLineNum.toString(), token.value, token.key])
                            bIdentifiedToken = true
                            break
                        }
                    }
                }

                // check for identifier
                if(!bIdentifiedToken && (stCode ==~ ~/[A-Za-z.]+\w*[\S\s]*/))
                {
                    String stMatched = (stCode =~ ~/[A-Za-z.]+\w*/)[0]
                    stCode = stCode.drop(stMatched.length())

                    // check for reserved words
                    for(def word in RESERVED_WORDS)
                    {
                        if(stMatched == word.key)
                        {
                            resultList.add([iLineNum.toString(), word.value, word.key])
                            iColumnNum += stMatched.length()
                            stMatched = null
                            break
                        }
                    }

                    // Identifier
                    if(stMatched != null)
                    {
                        resultList.add([iLineNum.toString(), "IDENTIFIER", stMatched])
                        iColumnNum += stMatched.length()
                    }

                    bIdentifiedToken = true
                }

                if(!bIdentifiedToken)
                {
                    iColumnNum += 1
                    resultList.add([iLineNum.toString(), "INVALID", stCode[0]])
                    stCode = stCode.drop(1)
                }
            }
        }
        return JsonOutput.toJson(resultList)
    }
}

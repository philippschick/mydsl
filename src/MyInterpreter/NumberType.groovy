package MyInterpreter

class NumberType extends DataType implements Comparable<NumberType> {
    private double value

    // Constructors

    NumberType(double num)
    {
        this.value = num
    }

    // Operators

    NumberType plus(NumberType num)
    {
        NumberType ret = new NumberType(this.value + num.value)

        if((this.timestamp != null) && (this.timestamp == num.timestamp))
            ret.timestamp = this.timestamp.copy()

        return ret
    }
    ListType plus(ListType list) { return new ListType(this, list.size()) + list }

    NumberType minus(NumberType num)
    {
        NumberType ret = new NumberType(this.value - num.value)

        if((this.timestamp != null) && (this.timestamp == num.timestamp))
            ret.timestamp = this.timestamp.copy()

        return ret
    }
    ListType minus(ListType list) { return new ListType(this, list.size()) - list }

    NumberType multiply(NumberType num)
    {
        NumberType ret = new NumberType(this.value * num.value)

        if((this.timestamp != null) && (this.timestamp == num.timestamp))
            ret.timestamp = this.timestamp.copy()

        return ret
    }
    ListType multiply(ListType list) { return new ListType(this, list.size()) * list }

    NumberType div(NumberType num)
    {
        NumberType ret = new NumberType(this.value / num.value)

        if((this.timestamp != null) && (this.timestamp == num.timestamp))
            ret.timestamp = this.timestamp.copy()

        return ret
    }
    ListType div(ListType list) { return new ListType(this, list.size()) / list }

    NumberType power(NumberType num)
    {
        NumberType ret = new NumberType(this.value ** num.value)

        if((this.timestamp != null) && (this.timestamp == num.timestamp))
            ret.timestamp = this.timestamp.copy()

        return ret
    }
    ListType power(ListType list) { return new ListType(this, list.size()) ** list }

    @Override
    int compareTo(NumberType num) { return this.value <=> num.value }

    BoolType isWithin(NumberType a, NumberType b) { return new BoolType((a <= this) && (this <= b)) }
    ListType isWithin(ListType a, ListType b) {
        if(a.size() != b.size())
            throw new Exception("The lists are of different lengths!")

        ListType ret = new ListType()
        ListType numList = new ListType(this, a.size())
        ListType resultA = a.compareElements(numList)
        ListType resultB = numList.compareElements(b)
        NumberType zero = new NumberType(0)

        for(int i = 0; i < a.size(); i++)
        {
            if((resultA[i] >= zero) || (resultB[i] <= zero))
                ret.add(new BoolType(true))
            else
                ret.add(new BoolType(false))
        }
        return ret
    }
    ListType isWithin(NumberType a, ListType b) { return this.isWithin(new ListType(a, b.size()), b) }
    ListType isWithin(ListType a, NumberType b) { return this.isWithin(a, new ListType(b, a.size())) }

    // utils

    protected double getValue() { return value }

    @Override
    String toString() { return this.value as String }

    @Override
    DataType copy()
    {
        NumberType ret = new NumberType(this.value)

        if(this.timestamp != null)
            ret.timestamp = this.timestamp.copy()
        return ret
    }
}
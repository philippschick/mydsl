package MyInterpreter

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.codehaus.groovy.runtime.typehandling.GroovyCastException

class Interpreter {
    static void run(String stCompilation)
    {
        eval(stCompilation)
    }

    private static def eval(String stIn)
    {
        def json = new JsonSlurper().parseText(stIn)

        try {
            switch (json.type) {
                case "STATEMENTBLOCK":
                    for (int i = 0; json.statements[i] != null; i++) {
                        eval(JsonOutput.toJson(json.statements[i]))
                    }
                    break

                case "WRITE":
                    return println(eval(JsonOutput.toJson(json.arg)))

                case "PLUS":
                    return eval(JsonOutput.toJson(json.arg[0])) + eval(JsonOutput.toJson(json.arg[1]))

                case "MINUS":
                    return eval(JsonOutput.toJson(json.arg[0])) - eval(JsonOutput.toJson(json.arg[1]))

                case "NUMTOKEN":
                    return new NumberType(json.value as int)

                case "STRTOKEN":
                    return new StringType(json.value as String)

                case "DIVIDE":
                    return eval(JsonOutput.toJson(json.arg[0])) / eval(JsonOutput.toJson(json.arg[1]))

                case "TIMES":
                    return eval(JsonOutput.toJson(json.arg[0])) * eval(JsonOutput.toJson(json.arg[1]))

                case "TRUE":
                    return new BoolType(true)

                case "FALSE":
                    return new BoolType(false)

                case "NULL":
                    return new NullType()

                case "NOT":
                    return eval(JsonOutput.toJson(json.arg[0])).not()

                case "AND":
                    return eval(JsonOutput.toJson(json.arg[0])) && eval(JsonOutput.toJson(json.arg[1]))

                case "OR":
                    return eval(JsonOutput.toJson(json.arg[0])) || eval(JsonOutput.toJson(json.arg[1]))

                case "EMPTYLIST":
                    return new ListType()

                case "LISTTOKEN":
                    ListType list = new ListType()
                    for (e in json.args) {
                        list.add(eval(JsonOutput.toJson(e)))
                    }
                    return list

                case "POWER":
                    return eval(JsonOutput.toJson(json.arg[0]))**eval(JsonOutput.toJson(json.arg[1]))

                case "ASSIGNMENT":
                    SymbolTable sym
                    DataType retVal

                    sym = SymbolTable.getSymbolTable()
                    retVal = eval(JsonOutput.toJson(json.arg))

                    sym.addSymbol(json.varname, retVal.copy())

                    return retVal

                case "VARIABLE":
                    return SymbolTable.getSymbolTable().getSymbol(json.name)

                case "SEQTO":
                    ListType ret = new ListType()
                    NumberType one = new NumberType(1)
                    NumberType value
                    NumberType end

                    try {
                        value = eval(JsonOutput.toJson(json.arg[0]))
                        end = eval(JsonOutput.toJson(json.arg[1]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    for (double i = value.getValue(); i <= end.getValue(); i++) {
                        ret.add(value)
                        value = value + one
                    }
                    return ret

                case "COUNTOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.size()

                case "MAXOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.getMax()

                case "MINOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.getMin()

                case "FIRSTOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.getFirst()

                case "LASTOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.getLast()

                case "SUMOP":
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.arg[0]))
                    }
                    catch (GroovyCastException e) {
                        return new NullType()
                    }

                    return list.getSum()

                case "FOR":
                    SymbolTable sym = SymbolTable.getSymbolTable()
                    String varName = json.varname
                    ListType list = new ListType()
                    try {
                        list = eval(JsonOutput.toJson(json.expression))
                    }
                    catch (GroovyCastException e) {
                        list = new ListType(e)
                    }

                    sym.addSymbol(json.varname, list.getFirst())

                    for (int i = 0; i < list.size(); i++) {
                        sym.addSymbol(varName, list[i])
                        eval(JsonOutput.toJson(json.statements))
                    }
                    sym.removeSymbol(varName)
                    break

                case "EQUAL":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) == eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "UNEQUAL":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) != eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "LT":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) < eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "LTEQ":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) <= eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "GT":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) > eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "GTEQ":
                    try {
                        return new BoolType(eval(JsonOutput.toJson(json.arg[0])) >= eval(JsonOutput.toJson(json.arg[1])))
                    }
                    catch (Exception e) {
                        return new NullType()
                    }

                case "WITHIN":
                    NumberType num = eval(JsonOutput.toJson(json.arg[0]))

                    return num.isWithin(eval(JsonOutput.toJson(json.arg[1])), eval(JsonOutput.toJson(json.arg[2])))

                case "NUMBER":
                    return MyInterpreter.NumberType

                case "TEXT":
                    return MyInterpreter.StringType

                case "BOOLEAN":
                    return MyInterpreter.BoolType

                case "LIST":
                    return MyInterpreter.ListType

                case "IS":
                    DataType value = eval(JsonOutput.toJson(json.arg[0]))
                    Class type = eval(JsonOutput.toJson(json.arg[1]))

                    if((value instanceof ListType) && (type != MyInterpreter.ListType))
                    {
                        // Treat element by element
                        return ((ListType) value).checkElementTypes(type)
                    }
                    else
                    {
                        return new BoolType(value.getClass() == type)
                    }

                case "TIMESTAMP":
                    return new DateTimeType(json.value)

                case "NOW":
                    return new DateTimeType()

                case "TIME":
                    DataType value = eval(JsonOutput.toJson(json.arg[0]))
                    return value.getTimestamp()

                case "SETTIME":
                    SymbolTable.getSymbolTable().getSymbol(json.varname).setTimestamp(eval(JsonOutput.toJson(json.arg)))
                    break

                case "EARLIEST":
                    ListType value = eval(JsonOutput.toJson(json.arg[0]))
                    return value.getEarliest()

                case "IF":
                    BoolType condition = eval(JsonOutput.toJson(json.condition))

                    if(condition == new BoolType(true))
                        eval(JsonOutput.toJson(json.thenbranch))
                    else
                        eval(JsonOutput.toJson(json.elsebranch))
                    break

                case "UMINUS":
                    return new NumberType(-1) * eval(JsonOutput.toJson(json.arg[0]))

                case "READ":
                    ListType ret = new ListType()
                    StringType path = eval(JsonOutput.toJson(json.arg[0]))
                    def csv = new File(path.toString()).readLines()*.split(",")

                    for(line in csv)
                    {
                        ListType list = new ListType()
                        for(elmnt in line)
                        {
                            DataType listElmnt

                            try
                            {
                                listElmnt = new NumberType(elmnt as double)
                            }
                            catch (NumberFormatException e)
                            {
                                listElmnt = new StringType(elmnt)
                            }
                            list.add(listElmnt)
                        }
                        ret.add(list)
                    }
                    return ret

                default:
                    print json
            }
        }
        catch (Exception e) { return new NullType() }
    }
}

package MyInterpreter

class ListType extends DataType {
    private ArrayList<DataType> internalList

    // Constructors

    ListType()
    {
        internalList = new ArrayList<DataType>()
    }

    ListType(DataType elmnt)
    {
        internalList = new ArrayList<DataType>()
        internalList.add(elmnt)
    }

    ListType(DataType elmnt, int size)
    {
        internalList = new ArrayList<DataType>()

        for(int i = 0; i < size; i++)
            internalList.add(elmnt)
    }

    // Operators

    ListType plus(NumberType num) { return this + new ListType(num, this.size()) }
    ListType plus(ListType list) { return this.binaryListOperator(list, {DataType elmnt0, DataType elmnt1 -> elmnt0 + elmnt1}) }

    ListType minus(NumberType num) { return this - new ListType(num, this.size()) }
    ListType minus(ListType list) { return this.binaryListOperator(list, {DataType elmnt0, DataType elmnt1 -> elmnt0 - elmnt1}) }

    ListType div(NumberType num) { return this / new ListType(num, this.size()) }
    ListType div(ListType list) { return this.binaryListOperator(list, {DataType elmnt0, DataType elmnt1 -> elmnt0 / elmnt1}) }

    ListType multiply(ListType list) { return this.binaryListOperator(list, {DataType elmnt0, DataType elmnt1 -> elmnt0 * elmnt1}) }
    ListType multiply(NumberType num) { return this * new ListType(num, this.size()) }

    ListType power(ListType list) { return this.binaryListOperator(list, {DataType elmnt0, DataType elmnt1 -> elmnt0 ** elmnt1}) }
    ListType power(NumberType num) { return this ** new ListType(num, this.size()) }

    DataType getAt(int i) { return internalList[i] }

    DataType getMax()
    {
        try
        {
            DataType ret = internalList[0]

            for(DataType e in internalList)
            {
                if((ret <=> e) < 0)
                    ret = e
                // else (compVal >= 0) --> do nothing
            }
            return ret
        }
        catch(Exception e)
        { return new NullType() }
    }

    DataType getMin()
    {
        try
        {
            DataType ret = internalList[0]

            for(DataType e in internalList)
            {
                if((ret <=> e) > 0)
                    ret = e
                // else (compVal <= 0) --> do nothing
            }
            return ret
        }
        catch(Exception e)
        { return new NullType() }
    }

    DataType getFirst() { return internalList.first() }
    DataType getLast() { return internalList.last() }

    DataType getSum()
    {
        try
        {
            DataType ret = internalList[0]

            for(int i = 1; i < internalList.size(); i++)
            {
                ret = ret + internalList[i]
            }
            return ret
            }
        catch(Exception e)
        { return new NullType() }
    }

    ListType compareElements(ListType o) { return binaryListOperator(o,{DataType elmnt0, DataType elmnt1 -> new NumberType(elmnt0 <=> elmnt1)}) }

    @Override
    boolean equals(o)
    {
        if(!(o instanceof  ListType))
            return false

        if(this.size() != o.size())
            return false

        for(int i = 0; i < this.size(); i++)
            if (this.internalList[i] != o.internalList[i])
                return false

        return true
    }

    ListType checkElementTypes(Class type)
    {
        ListType ret = new ListType()

        for(int i = 0; i < this.size(); i++)
            ret.add(new BoolType(this.internalList[i].getClass() == type))
        return ret
    }

    DataType getEarliest()
    {
        DataType ret = this.internalList[0]

        for(int i = 0; i < this.size(); i++)
        {
            if(ret.timestamp > this.internalList[i].timestamp)
                ret = this.internalList[i]
        }

        return ret
    }

    // utils

    void add(DataType e) { internalList.add(e) }
    int size() { return internalList.size() }

    private ListType binaryListOperator(ListType list, Closure operator)
    {
        ListType ret = new ListType()

        if(this.size() != list.size())
            return new NullType()

        for(int i = 0; i < this.size(); i++)
            ret.add(operator(this.internalList[i], list.internalList[i]))

        return ret
    }

    @Override
    String toString()
    {
        String stReturn = "["

        for(int i = 0; i < internalList.size(); i++)
        {
            stReturn += (i > 0) ? "," : ""
            stReturn += internalList[i].toString()
        }

        return stReturn + "]"
    }

    @Override
    DataType copy() {
        ListType ret = new ListType()

        ret.internalList = this.internalList.clone()

        return ret
    }

    @Override
    DataType getTimestamp()
    {
        ListType ret = new ListType()

        for (DataType e in this.internalList)
        {
            ret.add(e.getTimestamp())
        }

        return ret
    }

}

package MyInterpreter

import org.codehaus.groovy.runtime.typehandling.GroovyCastException

abstract class DataType {
    private DateTimeType timestamp

    // Operators

    def plus(def other) { return new NullType() }
    def minus(def other) { return new NullType() }
    def multiply(def other) { return new NullType() }
    def div(def other) { return new NullType() }
    def power(def other) { return new NullType() }
    @Override
    boolean equals(o) { this == o }
    def and(def other) { return new NullType() }
    def xor(def other) { return new NullType() }
    def or(def other) { return new NullType() }

    Object asType(Class type) { throw new GroovyCastException("Can't cast DataType!") }

    // utils

    void setTimestamp(DateTimeType time) { this.timestamp = time }
    DataType getTimestamp() { return timestamp }

    abstract DataType copy()
}

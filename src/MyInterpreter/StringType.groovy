package MyInterpreter

class StringType extends DataType implements Comparable<StringType> {
    private String value

    StringType(String s)
    {
        this.value = s
    }

    @Override
    String toString()
    {
        return value
    }

    // Operators
    StringType plus(StringType s) { return new StringType(this.value.concat(s.value)) }

    @Override
    int compareTo(StringType o) {
        return this.value <=> o.value
    }

    @Override
    DataType copy() { return new StringType(this.value) }
}

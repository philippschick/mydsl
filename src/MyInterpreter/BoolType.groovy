package MyInterpreter

import org.codehaus.groovy.runtime.typehandling.GroovyCastException

class BoolType extends DataType {
    private boolean value

    // Constructors

    BoolType(boolean b)
    {
        this.value = b
    }

    // Operators

    @Override
    boolean equals(o)
    {
        if(!(o instanceof  BoolType))
            return false

        return this.value == o.value
    }

    Object asType(Class type)
    {
        if(target == boolean)
            return this.value
        else
            throw new GroovyCastException("BoolType can be cast to boolean only!")
    }

    BoolType and(BoolType other) { return new BoolType(this.value & other.value) }
    BoolType xor(BoolType other) { return new BoolType(this.value ^ other.value) }
    BoolType or(BoolType other) { return new BoolType(this.value | other.value) }
    BoolType not() { return new BoolType(!(this.value)) }

    // utils

    @Override
    String toString() { return this.value as String }

    @Override
    DataType copy() { return new BoolType(this.value) }
}

package MyInterpreter

class NullType extends DataType {

    @Override
    String toString()
    {
        return "null"
    }

    @Override
    DataType copy() {
        return new NullType()
    }
}

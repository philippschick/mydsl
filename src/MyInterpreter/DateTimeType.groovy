package MyInterpreter

import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

class DateTimeType extends DataType implements Comparable<DateTimeType> {
    private LocalDateTime value

    // Constructors

    DateTimeType()
    {
        value = LocalDateTime.now()
    }

    DateTimeType(String time)
    {
        value = LocalDateTime.parse(time)
    }

    DateTimeType(LocalDateTime time)
    {
        value = time
    }

    // Operators

    @Override
    int compareTo(DateTimeType o) { return this.value <=> o.value }

    // utils

    @Override
    String toString()
    {
        return value.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    }

    @Override
    DataType copy() {
        return new DateTimeType(this.value)
    }
}

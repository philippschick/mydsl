package MyInterpreter

class SymbolTable {
    private static SymbolTable tableClass = null
    private HashMap<String, DataType> table = new HashMap<String, DataType>()

    private SymbolTable() { }

    static SymbolTable getSymbolTable()
    {
        if(tableClass == null)
        {
            tableClass = new SymbolTable()
        }
        return tableClass
    }

    void addSymbol(String ident, DataType value)
    {
        table.put(ident, value)
    }

    void removeSymbol(String ident)
    {
        table.remove(ident)
    }

    DataType getSymbol(String ident)
    {
        return table[ident]
    }
}

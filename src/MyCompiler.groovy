
class MyCompiler {
    static final String PARSER_PATH = "C:\\Users\\PhilippSchick\\IdeaProjects\\MyDSL\\DSM_Lemon\\parse.exe"

    static String compile(String stTokenstream)
    {
        String stReturn
        File file
        InputStream stdout
        Scanner scanner
        ProcessBuilder builder
        Process process

        stReturn = ""

        file = File.createTempFile("temp",".txt")
        file.deleteOnExit()

        file.write stTokenstream

        builder = new ProcessBuilder(PARSER_PATH)
        builder.redirectInput(file)
        process = builder.start()

        stdout = process.getInputStream()

        scanner = new Scanner(stdout)
        while (scanner.hasNextLine()) {
            stReturn = stReturn + scanner.nextLine()
        }

        return stReturn
    }
}

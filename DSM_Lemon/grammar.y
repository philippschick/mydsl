%include { 

#include "grammar.h"
#include <string.h>
#include <stdlib.h>
#include <string>
#include <cassert>
#include <iostream>
#include "cJSON.h"


using namespace std;
int get_token_id (char*);
const char *getValue (cJSON *token);
const char *getLine (cJSON *token);
cJSON *unary (char *fname, cJSON *a); 
cJSON *binary (char *fname, cJSON *a, cJSON *b); 
cJSON *ternary (char *fname, cJSON *a, cJSON *b, cJSON *c); 
char *linenumber;
char *curtoken;
char *curtype;
}  
   
   
%code {

using namespace std;
typedef struct {char *value; int line;} token;

token* create_token (char *value, int line) {
	token *t = (token*) malloc (sizeof (token));
	t->value = strdup (value);
	t->line = line;
	return t;
}

const char * getValue (cJSON* token) {
	return cJSON_GetObjectItem (token, "value")->valuestring;
}


const char * getLine (cJSON* token) {
	return cJSON_GetObjectItem (token, "line")->valuestring;
}


int main(int argc, char* argv[]) {
	char *result;
	std::string line;
	std::string input = "";
	while (std::getline(std::cin, line)) {
      input += line + "\n";
    }
	if (input == "") {
		cout << "Empty input";
		exit(0);
	}
	
	cJSON *root = cJSON_Parse(input.c_str());
	
	if (!root) {
		cout << "JSON invalid\n";
		exit(0);
	}
	
	void* pParser = ParseAlloc (malloc);
	int num = cJSON_GetArraySize (root);
	
	for (int i = 0; i < num; i++ ) {
	
		// Knoten im Token-Stream auslesen
		cJSON *node = cJSON_GetArrayItem(root,i);
		
		char *line = cJSON_GetArrayItem(node,0)->valuestring;
		char *type = cJSON_GetArrayItem(node,1)->valuestring;
		char *value = cJSON_GetArrayItem(node,2)->valuestring;
		
		cJSON *tok = cJSON_CreateObject();
		cJSON_AddStringToObject(tok, "value", value);
		cJSON_AddStringToObject(tok, "line", line);

		linenumber = line;
		curtoken = value;
		curtype = type;
		// THE und Kommentare werden ueberlesen
		if (strcmp(type, "THE") == 0) continue;
		if (strcmp(type, "COMMENT") == 0) continue;
		if (strcmp(type, "MCOMMENT") == 0) continue;
		
		int tokenid = get_token_id (type);
		Parse (pParser, tokenid, tok);
		
	}
	Parse (pParser, 0, 0);
    ParseFree(pParser, free );
}




/////////////////////// 
/////////////////////// 
// TOKENS
///////////////////////
/////////////////////// 

int get_token_id (char *token) {
    if (strcmp(token, "AND") == 0) return AND;
    if (strcmp(token, "ASSIGN") == 0) return ASSIGN;
    if (strcmp(token, "BOOLEAN") == 0) return BOOLEAN;
	if (strcmp(token, "COMMA") == 0) return COMMA;
	if (strcmp(token, "COUNTOP") == 0) return COUNTOP;
	if (strcmp(token, "DIVIDE") == 0) return DIVIDE;
	if (strcmp(token, "DO") == 0) return DO;
	if (strcmp(token, "EARLIEST") == 0) return EARLIEST;
	if (strcmp(token, "ELSE") == 0) return ELSE;
	if (strcmp(token, "ELSEIF") == 0) return ELSEIF;
	if (strcmp(token, "ENDDO") == 0) return ENDDO;
	if (strcmp(token, "ENDIF") == 0) return ENDIF;
	if (strcmp(token, "EQUAL") == 0) return EQUAL;
	if (strcmp(token, "FIRSTOP") == 0) return FIRSTOP;
	if (strcmp(token, "GT") == 0) return GT;
	if (strcmp(token, "GTEQ") == 0) return GTEQ;
	if (strcmp(token, "IDENTIFIER") == 0) return IDENTIFIER;
	if (strcmp(token, "IF") == 0) return IF;
	if (strcmp(token, "IN") == 0) return IN;
	if (strcmp(token, "IS") == 0) return IS;
	if (strcmp(token, "LPAR") == 0) return LPAR;
	if (strcmp(token, "LT") == 0) return LT;
	if (strcmp(token, "LTEQ") == 0) return LTEQ;
	if (strcmp(token, "RPAR") == 0) return RPAR;
	if (strcmp(token, "MINUS") == 0) return MINUS;
	if (strcmp(token, "NUMTOKEN") == 0) return NUMTOKEN;
	if (strcmp(token, "TRUE") == 0) return TRUE;
	if (strcmp(token, "FALSE") == 0) return FALSE;
	if (strcmp(token, "FOR") == 0) return FOR;
	if (strcmp(token, "LASTOP") == 0) return LASTOP;
	if (strcmp(token, "LBRACKET") == 0) return LBRACKET;
	if (strcmp(token, "LIST") == 0) return LIST;
	if (strcmp(token, "MAXOP") == 0) return MAXOP;
	if (strcmp(token, "MINOP") == 0) return MINOP;
	if (strcmp(token, "NOT") == 0) return NOT;
	if (strcmp(token, "NOW") == 0) return NOW;
	if (strcmp(token, "NULL") == 0) return NULLTOKEN;
	if (strcmp(token, "NUMBER") == 0) return NUMBER;
	if (strcmp(token, "OF") == 0) return OF;
	if (strcmp(token, "OR") == 0) return OR;
	if (strcmp(token, "PLUS") == 0) return PLUS;
	if (strcmp(token, "POWER") == 0) return POWER;
    if (strcmp(token, "RBRACKET") == 0) return RBRACKET;
    if (strcmp(token, "READ") == 0) return READ;
	if (strcmp(token, "SEMICOLON") == 0) return SEMICOLON;
	if (strcmp(token, "SEQTO") == 0) return SEQTO;
	if (strcmp(token, "STRTOKEN") == 0) return STRTOKEN;
	if (strcmp(token, "SUMOP") == 0) return SUMOP;
	if (strcmp(token, "TEXT") == 0) return TEXT;
	if (strcmp(token, "THEN") == 0) return THEN;
	if (strcmp(token, "TIME") == 0) return TIME;
    if (strcmp(token, "TIMES") == 0) return TIMES;
    if (strcmp(token, "TIMESTAMP") == 0) return TIMESTAMP;
    if (strcmp(token, "TO") == 0) return TO;
    if (strcmp(token, "UNEQUAL") == 0) return UNEQUAL;
	if (strcmp(token, "WRITE") == 0) return WRITE;
	if (strcmp(token, "WITHIN") == 0) return WITHIN;
	
	printf ("{\"error\" : true, \"message\": \"UNKNOWN TOKEN TYPE %s\"}\n", token);
	exit(0);
} 
  


cJSON* unary (char* fname, cJSON* a) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a);
	cJSON_AddStringToObject(res, "type", fname);
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
} 



cJSON* binary (char *fname, cJSON *a, cJSON *b) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a); 
	cJSON_AddItemToArray(arg, b);
	cJSON_AddStringToObject(res, "type", fname); 
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
}



cJSON* ternary (char *fname, cJSON *a, cJSON *b, cJSON *c) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a); 
	cJSON_AddItemToArray(arg, b); 
	cJSON_AddItemToArray(arg, c);
	cJSON_AddStringToObject(res, "type", fname); 
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
}



}
   
%syntax_error {
  printf ("{\"error\" : true, \"message\": \"Syntax Error: MyCompiler reports unexpected token \\\"%s\\\" of type \\\"%s\\\" in line %s\"}\n", curtoken, curtype, linenumber);
  exit(0);
} 
  
%token_type {cJSON *}
%default_type {cJSON *} 

/////////////////////// 
/////////////////////// 
// PRECEDENCE
///////////////////////
/////////////////////// 

%nonassoc  READ .
%right     EQUAL UNEQUAL LT LTEQ GT GTEQ EARLIEST .
%nonassoc  TIME OF IS WITHIN TO SEQTO MAXOP MINOP FIRSTOP LASTOP SUMOP COUNTOP .
%left 	   PLUS MINUS .
%left 	   TIMES DIVIDE .
%left      NOT AND OR .
%right     POWER .
%right     UMINUS .




/////////////////////// 
// CODE
///////////////////////


 
code ::= statementblock(sb) . 
{
	printf (cJSON_Print(sb)); 
}  



/////////////////////// 
// STATEMENTBLOCK
///////////////////////


statementblock(sb) ::= .
{
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "STATEMENTBLOCK");
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToObject(res, "statements", arg); 
	sb = res;
}


statementblock(sb) ::= statementblock(a) statement(b) .
{
	cJSON_AddItemToArray(cJSON_GetObjectItem ( a, "statements"), b);
	sb = a;
}



///////////////////////////
// WRITE
///////////////////////////

statement(r) ::= WRITE ex(e) SEMICOLON .
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON_AddStringToObject(res, "type", "WRITE"); 
	cJSON_AddItemToObject(res, "arg", e); 
	r = res; 
}




ex(r) ::= LPAR ex(a) RPAR .
{ 
	r = a; 
}


ex(r) ::= NUMTOKEN (a).        
{ 
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "NUMTOKEN");
	cJSON_AddStringToObject(res, "value", getValue(a)); 
	r = res; 
} 


ex(r) ::= STRTOKEN (a).        
{ 
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "STRTOKEN"); 
	cJSON_AddStringToObject(res, "value", getValue(a)); 
	r = res; 
}

ex(r) ::= TIMESTAMP (a).
{
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "TIMESTAMP");
	cJSON_AddStringToObject(res, "value", getValue(a));
	r = res;
}


ex(r) ::= IDENTIFIER (a) .
{ 
	cJSON *res = cJSON_CreateObject(); 
	cJSON_AddStringToObject(res, "type", "VARIABLE"); 
	cJSON_AddStringToObject(res, "name", getValue(a)); 
	cJSON_AddStringToObject(res, "line", getLine(a)); 
	r = res; 
}

ex(r) ::= TRUE (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "TRUE");
    cJSON_AddStringToObject(res, "value", "TRUE");
    r = res;
}

ex(r) ::= FALSE (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "FALSE");
    cJSON_AddStringToObject(res, "value", "FALSE");
    r = res;
}

ex(r) ::= NULLTOKEN (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NULL");
    cJSON_AddStringToObject(res, "value", "NULL");
    r = res;
}

ex(r) ::= NOW (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NOW");
    cJSON_AddStringToObject(res, "value", "NOW");
    r = res;
}

///////////////////////////
// OPERATORS
///////////////////////////

ex(r) ::= ex(a) PLUS ex(b) .                                
{r = binary ("PLUS", a, b); }

ex(r) ::= ex(a) MINUS ex(b) .                               
{r = binary ("MINUS", a, b); }

ex(r) ::= ex(a) TIMES ex(b) .                               
{r = binary ("TIMES", a, b); }

ex(r) ::= ex(a) DIVIDE ex(b) .                              
{r = binary ("DIVIDE", a, b); }

ex(r) ::= ex(a) POWER ex(b) .                               
{r = binary ("POWER", a, b); }

ex(r) ::= NOT ex(a) .
{r = unary ("NOT", a); }

ex(r) ::= ex(a) AND ex(b) .
{r = binary ("AND", a, b); }

ex(r) ::= ex(a) OR ex(b) .
{r = binary ("OR", a, b); }

ex(r) ::= ex(a) SEQTO ex(b) .
{r = binary ("SEQTO", a, b); }

ex(r) ::= MAXOP ex(a) .
{r = unary ("MAXOP", a); }

ex(r) ::= MINOP ex(a) .
{r = unary ("MINOP", a); }

ex(r) ::= FIRSTOP ex(a) .
{r = unary ("FIRSTOP", a); }

ex(r) ::= LASTOP ex(a) .
{r = unary ("LASTOP", a); }

ex(r) ::= SUMOP ex(a) .
{r = unary ("SUMOP", a); }

ex(r) ::= COUNTOP ex(a) .
{r = unary ("COUNTOP", a); }

ex(r) ::= ex(a) EQUAL ex(b) .
{r = binary ("EQUAL", a, b); }

ex(r) ::= ex(a) UNEQUAL ex(b) .
{r = binary ("UNEQUAL", a, b); }

ex(r) ::= ex(a) LT ex(b) .
{r = binary ("LT", a, b); }

ex(r) ::= ex(a) LTEQ ex(b) .
{r = binary ("LTEQ", a, b); }

ex(r) ::= ex(a) GT ex(b) .
{r = binary ("GT", a, b); }

ex(r) ::= ex(a) GTEQ ex(b) .
{r = binary ("GTEQ", a, b); }

ex(r) ::= NUMBER (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NUMBER");
    cJSON_AddStringToObject(res, "value", "NUMBER");
    r = res;
}

ex(r) ::= TEXT (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "TEXT");
    cJSON_AddStringToObject(res, "value", "TEXT");
    r = res;
}

ex(r) ::= LIST (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "LIST");
    cJSON_AddStringToObject(res, "value", "LIST");
    r = res;
}

ex(r) ::= BOOLEAN (a) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "BOOLEAN");
    cJSON_AddStringToObject(res, "value", "BOOLEAN");
    r = res;
}

ex(r) ::= ex(a) IS ex(b) .
{r = binary ("IS", a, b); }

ex(r) ::= ex(a) IS WITHIN ex(b) TO ex(c) .
{r = ternary ("WITHIN", a, b, c); }

ex(r) ::= EARLIEST ex(a) .
{r = unary ("EARLIEST", a); }

ex(r) ::= MINUS ex(a). [UMINUS]
{r = unary ("UMINUS", a); }

ex(r) ::= READ ex(a) .
{r = unary ("READ", a); }

///////////////////////////// TIME OF ///////////////////////////

ex(r) ::= TIME OF ex(a) .
{r = unary ("TIME", a); }

ex(r) ::= TIME ex(a) .
{r = unary ("TIME", a); }

statement(r) ::= TIME OF IDENTIFIER(a) ASSIGN ex(e) SEMICOLON .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "SETTIME");
    cJSON_AddStringToObject(res, "varname", getValue(a));
    cJSON_AddItemToObject(res, "arg", e); r = res;
}

///////////////////////////// ASSIGN ///////////////////////////
statement(r) ::= IDENTIFIER(a) ASSIGN ex(e) SEMICOLON . 
{ 
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "ASSIGNMENT");
    cJSON_AddStringToObject(res, "varname", getValue(a));
    cJSON_AddItemToObject(res, "arg", e); r = res;
}

// JSON-Arrays (Listen)
ex(r) ::= jsonarray (a) .
{r = a;}

jsonarray(r) ::= LBRACKET RBRACKET .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "EMPTYLIST");
    r = res;
}

// Nicht-leere Liste: Liste von Expressions, kommasepariert
jsonarray(r) ::= LBRACKET exlist(a) RBRACKET .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "LISTTOKEN");
    cJSON_AddItemToObject(res, "args", a);
    r = res;
}

// Eine einzelne Expression ist eine gültige Liste von Expressions
exlist(r) ::= ex(a) .
{
    cJSON *arg = cJSON_CreateArray();
    cJSON_AddItemToArray(arg, a); r = arg;
}

// Kommaseparierte Expressions
exlist(r) ::= exlist(a) COMMA ex(b) .
{
    cJSON_AddItemToArray(a,b);
    r = a;
}

///////////////////////////
// FOR
///////////////////////////

statement(r) ::= FOR IDENTIFIER(i) IN ex(e) DO statement(s) ENDDO SEMICOLON .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "FOR");
    cJSON_AddStringToObject(res, "varname", getValue(i));
    cJSON_AddItemToObject(res, "expression", e);
    cJSON_AddItemToObject(res, "statements", s);
    r = res;
}

///////////////////////////
// IF
///////////////////////////

statement(r) ::= IF if_then_else(a) .
{r = a;}

if_then_else(r) ::= ex(a) THEN statementblock(b) elseif(c) .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "IF");
    cJSON_AddItemToObject(res, "condition", a);
    cJSON_AddItemToObject(res, "thenbranch", (b));
    cJSON_AddItemToObject(res, "elsebranch", (c));
    r = res;
}

elseif(r) ::= ENDIF SEMICOLON .
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "STATEMENTBLOCK");
    cJSON *arg = cJSON_CreateArray();
    cJSON_AddItemToObject(res, "statements", arg);
    r = res;
}

elseif(r) ::= ELSE statementblock(a) ENDIF SEMICOLON .
{r = a;}

elseif(r) ::= ELSEIF if_then_else(a) .
{r = a;}
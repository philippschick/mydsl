/*
** 2000-05-29
**
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
*************************************************************************
** Driver template for the LEMON parser generator.
**
** The "lemon" program processes an LALR(1) input grammar file, then uses
** this template to construct a parser.  The "lemon" program inserts text
** at each "%%" line.  Also, any "P-a-r-s-e" identifer prefix (without the
** interstitial "-" characters) contained in this template is changed into
** the value of the %name directive from the grammar.  Otherwise, the content
** of this template is copied straight through into the generate parser
** source file.
**
** The following is the concatenation of all %include directives from the
** input grammar file:
*/
#include <stdio.h>
/************ Begin %include sections from the grammar ************************/
#line 1 "grammar.y"
 

#include "grammar.h"
#include <string.h>
#include <stdlib.h>
#include <string>
#include <cassert>
#include <iostream>
#include "cJSON.h"


using namespace std;
int get_token_id (char*);
const char *getValue (cJSON *token);
const char *getLine (cJSON *token);
cJSON *unary (char *fname, cJSON *a); 
cJSON *binary (char *fname, cJSON *a, cJSON *b); 
cJSON *ternary (char *fname, cJSON *a, cJSON *b, cJSON *c); 
char *linenumber;
char *curtoken;
char *curtype;
#line 50 "grammar.c"
/**************** End of %include directives **********************************/
/* These constants specify the various numeric values for terminal symbols
** in a format understandable to "makeheaders".  This section is blank unless
** "lemon" is run with the "-m" command-line option.
***************** Begin makeheaders token definitions *************************/
/**************** End makeheaders token definitions ***************************/

/* The next sections is a series of control #defines.
** various aspects of the generated parser.
**    YYCODETYPE         is the data type used to store the integer codes
**                       that represent terminal and non-terminal symbols.
**                       "unsigned char" is used if there are fewer than
**                       256 symbols.  Larger types otherwise.
**    YYNOCODE           is a number of type YYCODETYPE that is not used for
**                       any terminal or nonterminal symbol.
**    YYFALLBACK         If defined, this indicates that one or more tokens
**                       (also known as: "terminal symbols") have fall-back
**                       values which should be used if the original symbol
**                       would not parse.  This permits keywords to sometimes
**                       be used as identifiers, for example.
**    YYACTIONTYPE       is the data type used for "action codes" - numbers
**                       that indicate what to do in response to the next
**                       token.
**    ParseTOKENTYPE     is the data type used for minor type for terminal
**                       symbols.  Background: A "minor type" is a semantic
**                       value associated with a terminal or non-terminal
**                       symbols.  For example, for an "ID" terminal symbol,
**                       the minor type might be the name of the identifier.
**                       Each non-terminal can have a different minor type.
**                       Terminal symbols all have the same minor type, though.
**                       This macros defines the minor type for terminal 
**                       symbols.
**    YYMINORTYPE        is the data type used for all minor types.
**                       This is typically a union of many types, one of
**                       which is ParseTOKENTYPE.  The entry in the union
**                       for terminal symbols is called "yy0".
**    YYSTACKDEPTH       is the maximum depth of the parser's stack.  If
**                       zero the stack is dynamically sized using realloc()
**    ParseARG_SDECL     A static variable declaration for the %extra_argument
**    ParseARG_PDECL     A parameter declaration for the %extra_argument
**    ParseARG_STORE     Code to store %extra_argument into yypParser
**    ParseARG_FETCH     Code to extract %extra_argument from yypParser
**    YYERRORSYMBOL      is the code number of the error symbol.  If not
**                       defined, then do no error processing.
**    YYNSTATE           the combined number of states.
**    YYNRULE            the number of rules in the grammar
**    YY_MAX_SHIFT       Maximum value for shift actions
**    YY_MIN_SHIFTREDUCE Minimum value for shift-reduce actions
**    YY_MAX_SHIFTREDUCE Maximum value for shift-reduce actions
**    YY_MIN_REDUCE      Maximum value for reduce actions
**    YY_ERROR_ACTION    The yy_action[] code for syntax error
**    YY_ACCEPT_ACTION   The yy_action[] code for accept
**    YY_NO_ACTION       The yy_action[] code for no-op
*/
#ifndef INTERFACE
# define INTERFACE 1
#endif
/************* Begin control #defines *****************************************/
#define YYCODETYPE unsigned char
#define YYNOCODE 69
#define YYACTIONTYPE unsigned char
#define ParseTOKENTYPE cJSON *
typedef union {
  int yyinit;
  ParseTOKENTYPE yy0;
} YYMINORTYPE;
#ifndef YYSTACKDEPTH
#define YYSTACKDEPTH 100
#endif
#define ParseARG_SDECL
#define ParseARG_PDECL
#define ParseARG_FETCH
#define ParseARG_STORE
#define YYNSTATE             92
#define YYNRULE              58
#define YY_MAX_SHIFT         91
#define YY_MIN_SHIFTREDUCE   119
#define YY_MAX_SHIFTREDUCE   176
#define YY_MIN_REDUCE        177
#define YY_MAX_REDUCE        234
#define YY_ERROR_ACTION      235
#define YY_ACCEPT_ACTION     236
#define YY_NO_ACTION         237
/************* End control #defines *******************************************/

/* Define the yytestcase() macro to be a no-op if is not already defined
** otherwise.
**
** Applications can choose to define yytestcase() in the %include section
** to a macro that can assist in verifying code coverage.  For production
** code the yytestcase() macro should be turned off.  But it is useful
** for testing.
*/
#ifndef yytestcase
# define yytestcase(X)
#endif


/* Next are the tables used to determine what action to take based on the
** current state and lookahead token.  These tables are used to implement
** functions that take a state number and lookahead value and return an
** action integer.  
**
** Suppose the action integer is N.  Then the action is determined as
** follows
**
**   0 <= N <= YY_MAX_SHIFT             Shift N.  That is, push the lookahead
**                                      token onto the stack and goto state N.
**
**   N between YY_MIN_SHIFTREDUCE       Shift to an arbitrary state then
**     and YY_MAX_SHIFTREDUCE           reduce by rule N-YY_MIN_SHIFTREDUCE.
**
**   N between YY_MIN_REDUCE            Reduce by rule N-YY_MIN_REDUCE
**     and YY_MAX_REDUCE

**   N == YY_ERROR_ACTION               A syntax error has occurred.
**
**   N == YY_ACCEPT_ACTION              The parser accepts its input.
**
**   N == YY_NO_ACTION                  No such action.  Denotes unused
**                                      slots in the yy_action[] table.
**
** The action table is constructed as a single large table named yy_action[].
** Given state S and lookahead X, the action is computed as
**
**      yy_action[ yy_shift_ofst[S] + X ]
**
** If the index value yy_shift_ofst[S]+X is out of range or if the value
** yy_lookahead[yy_shift_ofst[S]+X] is not equal to X or if yy_shift_ofst[S]
** is equal to YY_SHIFT_USE_DFLT, it means that the action is not in the table
** and that yy_default[S] should be used instead.  
**
** The formula above is for computing the action when the lookahead is
** a terminal symbol.  If the lookahead is a non-terminal (as occurs after
** a reduce action) then the yy_reduce_ofst[] array is used in place of
** the yy_shift_ofst[] array and YY_REDUCE_USE_DFLT is used in place of
** YY_SHIFT_USE_DFLT.
**
** The following are the tables generated in this section:
**
**  yy_action[]        A single table containing all actions.
**  yy_lookahead[]     A table containing the lookahead for each entry in
**                     yy_action.  Used to detect hash collisions.
**  yy_shift_ofst[]    For each state, the offset into yy_action for
**                     shifting terminals.
**  yy_reduce_ofst[]   For each state, the offset into yy_action for
**                     shifting non-terminals after a reduce.
**  yy_default[]       Default action for each state.
**
*********** Begin parsing tables **********************************************/
#define YY_ACTTAB_COUNT (483)
static const YYACTIONTYPE yy_action[] = {
 /*     0 */    13,   34,   33,  121,   31,   30,   32,   15,    4,   53,
 /*    10 */   166,   77,  236,   41,   21,   20,   19,   18,   17,   16,
 /*    20 */    81,   14,   45,  166,   22,  176,   40,   87,   39,   45,
 /*    30 */   166,   36,  172,  124,  125,  126,  127,  128,  129,  130,
 /*    40 */   131,  153,  154,  155,  156,   13,    1,  167,   38,   31,
 /*    50 */    30,   32,   15,    4,   12,   84,   64,  166,  175,   21,
 /*    60 */    20,   19,   18,   17,   16,  121,   14,  174,   83,   22,
 /*    70 */   173,   87,    3,  171,   78,   80,   36,    6,  124,  125,
 /*    80 */   126,  127,  128,  129,  130,  131,  153,  154,  155,  156,
 /*    90 */    13,    1,   38,   71,  166,   46,  166,   15,    4,   84,
 /*   100 */    82,   10,   47,  166,   21,   20,   19,   18,   17,   16,
 /*   110 */     7,   14,   83,    8,   22,   85,    3,   87,   79,   43,
 /*   120 */     2,   36,   86,  124,  125,  126,  127,  128,  129,  130,
 /*   130 */   131,  153,  154,  155,  156,   13,    1,   32,   38,  179,
 /*   140 */    48,  166,   15,    4,  179,   84,   62,  166,  179,   21,
 /*   150 */    20,   19,   18,   17,   16,  179,   14,  179,   83,   22,
 /*   160 */    49,  166,    3,   52,  166,  179,   36,  179,  124,  125,
 /*   170 */   126,  127,  128,  129,  130,  131,  153,  154,  155,  156,
 /*   180 */   179,    1,   28,   27,   26,   25,   24,   23,   63,  166,
 /*   190 */   179,    5,   54,  166,   29,  160,  166,  179,   55,  166,
 /*   200 */   179,   37,   35,   34,   33,  179,   31,   30,   32,   65,
 /*   210 */   166,   66,  166,  235,   67,  166,  235,   68,  166,  179,
 /*   220 */    69,  166,  179,   37,   35,   34,   33,  179,   31,   30,
 /*   230 */    32,  179,   70,  166,  179,   44,   88,  166,   28,   27,
 /*   240 */    26,   25,   24,   23,  179,   56,  166,    5,   57,  166,
 /*   250 */    29,   58,  166,   59,  166,   60,  166,   37,   35,   34,
 /*   260 */    33,  179,   31,   30,   32,   28,   27,   26,   25,   24,
 /*   270 */    23,   61,  166,  179,    5,   72,  166,   29,   89,  166,
 /*   280 */    90,  166,   91,  166,   37,   35,   34,   33,   42,   31,
 /*   290 */    30,   32,   75,  166,  165,  179,  179,   28,   27,   26,
 /*   300 */    25,   24,   23,   76,  166,  179,    5,   74,  166,   29,
 /*   310 */    50,  166,   73,  166,   51,  166,   37,   35,   34,   33,
 /*   320 */   179,   31,   30,   32,  168,   11,  164,  179,  179,   28,
 /*   330 */    27,   26,   25,   24,   23,  179,  179,  179,    5,  179,
 /*   340 */     9,   29,  179,  179,  179,  179,  179,  179,   37,   35,
 /*   350 */    34,   33,  179,   31,   30,   32,   28,   27,   26,   25,
 /*   360 */    24,   23,  179,  179,  179,    5,  179,  179,   29,  179,
 /*   370 */   179,  179,  179,  179,  179,   37,   35,   34,   33,  179,
 /*   380 */    31,   30,   32,  179,  179,  179,  179,  123,  179,  179,
 /*   390 */    28,   27,   26,   25,   24,   23,  179,  179,  179,    5,
 /*   400 */   179,  179,   29,  179,  179,  179,  179,  179,  179,   37,
 /*   410 */    35,   34,   33,  179,   31,   30,   32,  179,  179,  122,
 /*   420 */   179,  179,   28,   27,   26,   25,   24,   23,  177,  179,
 /*   430 */   179,    5,  179,  179,   29,  179,  179,   87,  179,  179,
 /*   440 */   179,   37,   35,   34,   33,  179,   31,   30,   32,  179,
 /*   450 */   179,  179,  179,  179,  179,  179,  179,  179,   38,  179,
 /*   460 */   179,  179,  179,  179,  179,   84,  179,  179,  179,  179,
 /*   470 */   179,  179,  179,  179,  179,  179,  179,  179,   83,  179,
 /*   480 */   179,  179,    3,
};
static const YYCODETYPE yy_lookahead[] = {
 /*     0 */     1,   23,   24,   62,   26,   27,   28,    8,    9,   63,
 /*    10 */    64,   65,   60,   61,   15,   16,   17,   18,   19,   20,
 /*    20 */    62,   22,   63,   64,   25,   66,   61,    9,   61,   63,
 /*    30 */    64,   32,   66,   34,   35,   36,   37,   38,   39,   40,
 /*    40 */    41,   42,   43,   44,   45,    1,   47,   48,   30,   26,
 /*    50 */    27,   28,    8,    9,   10,   37,   63,   64,   31,   15,
 /*    60 */    16,   17,   18,   19,   20,   62,   22,   31,   50,   25,
 /*    70 */    67,    9,   54,   31,   56,   53,   32,   51,   34,   35,
 /*    80 */    36,   37,   38,   39,   40,   41,   42,   43,   44,   45,
 /*    90 */     1,   47,   30,   63,   64,   63,   64,    8,    9,   37,
 /*   100 */    37,   12,   63,   64,   15,   16,   17,   18,   19,   20,
 /*   110 */    46,   22,   50,   46,   25,   37,   54,    9,   56,   57,
 /*   120 */    58,   32,   10,   34,   35,   36,   37,   38,   39,   40,
 /*   130 */    41,   42,   43,   44,   45,    1,   47,   28,   30,   68,
 /*   140 */    63,   64,    8,    9,   68,   37,   63,   64,   68,   15,
 /*   150 */    16,   17,   18,   19,   20,   68,   22,   68,   50,   25,
 /*   160 */    63,   64,   54,   63,   64,   68,   32,   68,   34,   35,
 /*   170 */    36,   37,   38,   39,   40,   41,   42,   43,   44,   45,
 /*   180 */    68,   47,    2,    3,    4,    5,    6,    7,   63,   64,
 /*   190 */    68,   11,   63,   64,   14,   63,   64,   68,   63,   64,
 /*   200 */    68,   21,   22,   23,   24,   68,   26,   27,   28,   63,
 /*   210 */    64,   63,   64,   11,   63,   64,   14,   63,   64,   68,
 /*   220 */    63,   64,   68,   21,   22,   23,   24,   68,   26,   27,
 /*   230 */    28,   68,   63,   64,   68,   55,   63,   64,    2,    3,
 /*   240 */     4,    5,    6,    7,   68,   63,   64,   11,   63,   64,
 /*   250 */    14,   63,   64,   63,   64,   63,   64,   21,   22,   23,
 /*   260 */    24,   68,   26,   27,   28,    2,    3,    4,    5,    6,
 /*   270 */     7,   63,   64,   68,   11,   63,   64,   14,   63,   64,
 /*   280 */    63,   64,   63,   64,   21,   22,   23,   24,   52,   26,
 /*   290 */    27,   28,   63,   64,   31,   68,   68,    2,    3,    4,
 /*   300 */     5,    6,    7,   63,   64,   68,   11,   63,   64,   14,
 /*   310 */    63,   64,   63,   64,   63,   64,   21,   22,   23,   24,
 /*   320 */    68,   26,   27,   28,   48,   49,   31,   68,   68,    2,
 /*   330 */     3,    4,    5,    6,    7,   68,   68,   68,   11,   68,
 /*   340 */    13,   14,   68,   68,   68,   68,   68,   68,   21,   22,
 /*   350 */    23,   24,   68,   26,   27,   28,    2,    3,    4,    5,
 /*   360 */     6,    7,   68,   68,   68,   11,   68,   68,   14,   68,
 /*   370 */    68,   68,   68,   68,   68,   21,   22,   23,   24,   68,
 /*   380 */    26,   27,   28,   68,   68,   68,   68,   33,   68,   68,
 /*   390 */     2,    3,    4,    5,    6,    7,   68,   68,   68,   11,
 /*   400 */    68,   68,   14,   68,   68,   68,   68,   68,   68,   21,
 /*   410 */    22,   23,   24,   68,   26,   27,   28,   68,   68,   31,
 /*   420 */    68,   68,    2,    3,    4,    5,    6,    7,    0,   68,
 /*   430 */    68,   11,   68,   68,   14,   68,   68,    9,   68,   68,
 /*   440 */    68,   21,   22,   23,   24,   68,   26,   27,   28,   68,
 /*   450 */    68,   68,   68,   68,   68,   68,   68,   68,   30,   68,
 /*   460 */    68,   68,   68,   68,   68,   37,   68,   68,   68,   68,
 /*   470 */    68,   68,   68,   68,   68,   68,   68,   68,   50,   68,
 /*   480 */    68,   68,   54,
};
#define YY_SHIFT_USE_DFLT (-23)
#define YY_SHIFT_COUNT (91)
#define YY_SHIFT_MIN   (-22)
#define YY_SHIFT_MAX   (428)
static const short yy_shift_ofst[] = {
 /*     0 */   -23,   -1,  134,  134,   44,   89,  134,  134,  134,  134,
 /*    10 */   134,  134,  134,  134,  134,  134,  134,  134,  134,  134,
 /*    20 */   134,  134,  134,  134,  134,  134,  134,  134,  134,  134,
 /*    30 */   134,  134,  134,  134,  134,  134,  134,  134,  134,   62,
 /*    40 */    18,  428,  108,  -23,  -23,  180,  236,  263,  295,  327,
 /*    50 */   354,  388,  420,  420,  420,  420,  420,  420,  420,  420,
 /*    60 */   420,  420,  202,  202,  202,  202,  202,  202,  202,  202,
 /*    70 */   202,  202,  202,  -22,  -22,   23,   23,  276,   27,   36,
 /*    80 */    42,   22,   26,   63,   64,   67,   78,  112,  109,  109,
 /*    90 */   109,  109,
};
#define YY_REDUCE_USE_DFLT (-60)
#define YY_REDUCE_COUNT (44)
#define YY_REDUCE_MIN   (-59)
#define YY_REDUCE_MAX   (251)
static const short yy_reduce_ofst[] = {
 /*     0 */   -48,  -54,  -41,  -34,   -7,   30,   32,   39,   77,   83,
 /*    10 */    97,  100,  125,  129,  132,  135,  146,  148,  151,  154,
 /*    20 */   157,  169,  173,  182,  185,  188,  190,  192,  208,  212,
 /*    30 */   215,  217,  219,  229,  240,  244,  247,  249,  251,    3,
 /*    40 */   -59,  -59,  -42,  -35,  -33,
};
static const YYACTIONTYPE yy_default[] = {
 /*     0 */   178,  235,  235,  235,  235,  235,  235,  235,  235,  235,
 /*    10 */   235,  235,  235,  235,  235,  235,  235,  235,  235,  235,
 /*    20 */   235,  235,  235,  235,  235,  235,  235,  235,  235,  235,
 /*    30 */   235,  235,  235,  235,  235,  235,  235,  235,  235,  235,
 /*    40 */   235,  235,  235,  178,  178,  235,  235,  235,  235,  235,
 /*    50 */   235,  235,  228,  227,  219,  217,  210,  209,  208,  207,
 /*    60 */   206,  205,  216,  220,  221,  204,  203,  202,  201,  200,
 /*    70 */   199,  215,  198,  190,  191,  193,  192,  235,  235,  235,
 /*    80 */   235,  235,  235,  235,  235,  235,  235,  235,  195,  197,
 /*    90 */   196,  194,
};
/********** End of lemon-generated parsing tables *****************************/

/* The next table maps tokens (terminal symbols) into fallback tokens.  
** If a construct like the following:
** 
**      %fallback ID X Y Z.
**
** appears in the grammar, then ID becomes a fallback token for X, Y,
** and Z.  Whenever one of the tokens X, Y, or Z is input to the parser
** but it does not parse, the type of the token is changed to ID and
** the parse is retried before an error is thrown.
**
** This feature can be used, for example, to cause some keywords in a language
** to revert to identifiers if they keyword does not apply in the context where
** it appears.
*/
#ifdef YYFALLBACK
static const YYCODETYPE yyFallback[] = {
};
#endif /* YYFALLBACK */

/* The following structure represents a single element of the
** parser's stack.  Information stored includes:
**
**   +  The state number for the parser at this level of the stack.
**
**   +  The value of the token stored at this level of the stack.
**      (In other words, the "major" token.)
**
**   +  The semantic value stored at this level of the stack.  This is
**      the information used by the action routines in the grammar.
**      It is sometimes called the "minor" token.
**
** After the "shift" half of a SHIFTREDUCE action, the stateno field
** actually contains the reduce action for the second half of the
** SHIFTREDUCE.
*/
struct yyStackEntry {
  YYACTIONTYPE stateno;  /* The state-number, or reduce action in SHIFTREDUCE */
  YYCODETYPE major;      /* The major token value.  This is the code
                         ** number for the token at this stack level */
  YYMINORTYPE minor;     /* The user-supplied minor token value.  This
                         ** is the value of the token  */
};
typedef struct yyStackEntry yyStackEntry;

/* The state of the parser is completely contained in an instance of
** the following structure */
struct yyParser {
  yyStackEntry *yytos;          /* Pointer to top element of the stack */
#ifdef YYTRACKMAXSTACKDEPTH
  int yyhwm;                    /* High-water mark of the stack */
#endif
#ifndef YYNOERRORRECOVERY
  int yyerrcnt;                 /* Shifts left before out of the error */
#endif
  ParseARG_SDECL                /* A place to hold %extra_argument */
#if YYSTACKDEPTH<=0
  int yystksz;                  /* Current side of the stack */
  yyStackEntry *yystack;        /* The parser's stack */
  yyStackEntry yystk0;          /* First stack entry */
#else
  yyStackEntry yystack[YYSTACKDEPTH];  /* The parser's stack */
#endif
};
typedef struct yyParser yyParser;

#ifndef NDEBUG
#include <stdio.h>
static FILE *yyTraceFILE = 0;
static char *yyTracePrompt = 0;
#endif /* NDEBUG */

#ifndef NDEBUG
/* 
** Turn parser tracing on by giving a stream to which to write the trace
** and a prompt to preface each trace message.  Tracing is turned off
** by making either argument NULL 
**
** Inputs:
** <ul>
** <li> A FILE* to which trace output should be written.
**      If NULL, then tracing is turned off.
** <li> A prefix string written at the beginning of every
**      line of trace output.  If NULL, then tracing is
**      turned off.
** </ul>
**
** Outputs:
** None.
*/
void ParseTrace(FILE *TraceFILE, char *zTracePrompt){
  yyTraceFILE = TraceFILE;
  yyTracePrompt = zTracePrompt;
  if( yyTraceFILE==0 ) yyTracePrompt = 0;
  else if( yyTracePrompt==0 ) yyTraceFILE = 0;
}
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing shifts, the names of all terminals and nonterminals
** are required.  The following table supplies these names */
static const char *const yyTokenName[] = { 
  "$",             "READ",          "EQUAL",         "UNEQUAL",     
  "LT",            "LTEQ",          "GT",            "GTEQ",        
  "EARLIEST",      "TIME",          "OF",            "IS",          
  "WITHIN",        "TO",            "SEQTO",         "MAXOP",       
  "MINOP",         "FIRSTOP",       "LASTOP",        "SUMOP",       
  "COUNTOP",       "PLUS",          "MINUS",         "TIMES",       
  "DIVIDE",        "NOT",           "AND",           "OR",          
  "POWER",         "UMINUS",        "WRITE",         "SEMICOLON",   
  "LPAR",          "RPAR",          "NUMTOKEN",      "STRTOKEN",    
  "TIMESTAMP",     "IDENTIFIER",    "TRUE",          "FALSE",       
  "NULLTOKEN",     "NOW",           "NUMBER",        "TEXT",        
  "LIST",          "BOOLEAN",       "ASSIGN",        "LBRACKET",    
  "RBRACKET",      "COMMA",         "FOR",           "IN",          
  "DO",            "ENDDO",         "IF",            "THEN",        
  "ENDIF",         "ELSE",          "ELSEIF",        "error",       
  "code",          "statementblock",  "statement",     "ex",          
  "jsonarray",     "exlist",        "if_then_else",  "elseif",      
};
#endif /* NDEBUG */

#ifndef NDEBUG
/* For tracing reduce actions, the names of all rules are required.
*/
static const char *const yyRuleName[] = {
 /*   0 */ "code ::= statementblock",
 /*   1 */ "statementblock ::=",
 /*   2 */ "statementblock ::= statementblock statement",
 /*   3 */ "statement ::= WRITE ex SEMICOLON",
 /*   4 */ "ex ::= LPAR ex RPAR",
 /*   5 */ "ex ::= NUMTOKEN",
 /*   6 */ "ex ::= STRTOKEN",
 /*   7 */ "ex ::= TIMESTAMP",
 /*   8 */ "ex ::= IDENTIFIER",
 /*   9 */ "ex ::= TRUE",
 /*  10 */ "ex ::= FALSE",
 /*  11 */ "ex ::= NULLTOKEN",
 /*  12 */ "ex ::= NOW",
 /*  13 */ "ex ::= ex PLUS ex",
 /*  14 */ "ex ::= ex MINUS ex",
 /*  15 */ "ex ::= ex TIMES ex",
 /*  16 */ "ex ::= ex DIVIDE ex",
 /*  17 */ "ex ::= ex POWER ex",
 /*  18 */ "ex ::= NOT ex",
 /*  19 */ "ex ::= ex AND ex",
 /*  20 */ "ex ::= ex OR ex",
 /*  21 */ "ex ::= ex SEQTO ex",
 /*  22 */ "ex ::= MAXOP ex",
 /*  23 */ "ex ::= MINOP ex",
 /*  24 */ "ex ::= FIRSTOP ex",
 /*  25 */ "ex ::= LASTOP ex",
 /*  26 */ "ex ::= SUMOP ex",
 /*  27 */ "ex ::= COUNTOP ex",
 /*  28 */ "ex ::= ex EQUAL ex",
 /*  29 */ "ex ::= ex UNEQUAL ex",
 /*  30 */ "ex ::= ex LT ex",
 /*  31 */ "ex ::= ex LTEQ ex",
 /*  32 */ "ex ::= ex GT ex",
 /*  33 */ "ex ::= ex GTEQ ex",
 /*  34 */ "ex ::= NUMBER",
 /*  35 */ "ex ::= TEXT",
 /*  36 */ "ex ::= LIST",
 /*  37 */ "ex ::= BOOLEAN",
 /*  38 */ "ex ::= ex IS ex",
 /*  39 */ "ex ::= ex IS WITHIN ex TO ex",
 /*  40 */ "ex ::= EARLIEST ex",
 /*  41 */ "ex ::= MINUS ex",
 /*  42 */ "ex ::= READ ex",
 /*  43 */ "ex ::= TIME OF ex",
 /*  44 */ "ex ::= TIME ex",
 /*  45 */ "statement ::= TIME OF IDENTIFIER ASSIGN ex SEMICOLON",
 /*  46 */ "statement ::= IDENTIFIER ASSIGN ex SEMICOLON",
 /*  47 */ "ex ::= jsonarray",
 /*  48 */ "jsonarray ::= LBRACKET RBRACKET",
 /*  49 */ "jsonarray ::= LBRACKET exlist RBRACKET",
 /*  50 */ "exlist ::= ex",
 /*  51 */ "exlist ::= exlist COMMA ex",
 /*  52 */ "statement ::= FOR IDENTIFIER IN ex DO statement ENDDO SEMICOLON",
 /*  53 */ "statement ::= IF if_then_else",
 /*  54 */ "if_then_else ::= ex THEN statementblock elseif",
 /*  55 */ "elseif ::= ENDIF SEMICOLON",
 /*  56 */ "elseif ::= ELSE statementblock ENDIF SEMICOLON",
 /*  57 */ "elseif ::= ELSEIF if_then_else",
};
#endif /* NDEBUG */


#if YYSTACKDEPTH<=0
/*
** Try to increase the size of the parser stack.  Return the number
** of errors.  Return 0 on success.
*/
static int yyGrowStack(yyParser *p){
  int newSize;
  int idx;
  yyStackEntry *pNew;

  newSize = p->yystksz*2 + 100;
  idx = p->yytos ? (int)(p->yytos - p->yystack) : 0;
  if( p->yystack==&p->yystk0 ){
    pNew = malloc(newSize*sizeof(pNew[0]));
    if( pNew ) pNew[0] = p->yystk0;
  }else{
    pNew = realloc(p->yystack, newSize*sizeof(pNew[0]));
  }
  if( pNew ){
    p->yystack = pNew;
    p->yytos = &p->yystack[idx];
#ifndef NDEBUG
    if( yyTraceFILE ){
      fprintf(yyTraceFILE,"%sStack grows from %d to %d entries.\n",
              yyTracePrompt, p->yystksz, newSize);
    }
#endif
    p->yystksz = newSize;
  }
  return pNew==0; 
}
#endif

/* Datatype of the argument to the memory allocated passed as the
** second argument to ParseAlloc() below.  This can be changed by
** putting an appropriate #define in the %include section of the input
** grammar.
*/
#ifndef YYMALLOCARGTYPE
# define YYMALLOCARGTYPE size_t
#endif

/* 
** This function allocates a new parser.
** The only argument is a pointer to a function which works like
** malloc.
**
** Inputs:
** A pointer to the function used to allocate memory.
**
** Outputs:
** A pointer to a parser.  This pointer is used in subsequent calls
** to Parse and ParseFree.
*/
void *ParseAlloc(void *(*mallocProc)(YYMALLOCARGTYPE)){
  yyParser *pParser;
  pParser = (yyParser*)(*mallocProc)( (YYMALLOCARGTYPE)sizeof(yyParser) );
  if( pParser ){
#ifdef YYTRACKMAXSTACKDEPTH
    pParser->yyhwm = 0;
#endif
#if YYSTACKDEPTH<=0
    pParser->yytos = NULL;
    pParser->yystack = NULL;
    pParser->yystksz = 0;
    if( yyGrowStack(pParser) ){
      pParser->yystack = &pParser->yystk0;
      pParser->yystksz = 1;
    }
#endif
#ifndef YYNOERRORRECOVERY
    pParser->yyerrcnt = -1;
#endif
    pParser->yytos = pParser->yystack;
    pParser->yystack[0].stateno = 0;
    pParser->yystack[0].major = 0;
  }
  return pParser;
}

/* The following function deletes the "minor type" or semantic value
** associated with a symbol.  The symbol can be either a terminal
** or nonterminal. "yymajor" is the symbol code, and "yypminor" is
** a pointer to the value to be deleted.  The code used to do the 
** deletions is derived from the %destructor and/or %token_destructor
** directives of the input grammar.
*/
static void yy_destructor(
  yyParser *yypParser,    /* The parser */
  YYCODETYPE yymajor,     /* Type code for object to destroy */
  YYMINORTYPE *yypminor   /* The object to be destroyed */
){
  ParseARG_FETCH;
  switch( yymajor ){
    /* Here is inserted the actions which take place when a
    ** terminal or non-terminal is destroyed.  This can happen
    ** when the symbol is popped from the stack during a
    ** reduce or during error processing or when a parser is 
    ** being destroyed before it is finished parsing.
    **
    ** Note: during a reduce, the only symbols destroyed are those
    ** which appear on the RHS of the rule, but which are *not* used
    ** inside the C code.
    */
/********* Begin destructor definitions ***************************************/
/********* End destructor definitions *****************************************/
    default:  break;   /* If no destructor action specified: do nothing */
  }
}

/*
** Pop the parser's stack once.
**
** If there is a destructor routine associated with the token which
** is popped from the stack, then call it.
*/
static void yy_pop_parser_stack(yyParser *pParser){
  yyStackEntry *yytos;
  assert( pParser->yytos!=0 );
  assert( pParser->yytos > pParser->yystack );
  yytos = pParser->yytos--;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sPopping %s\n",
      yyTracePrompt,
      yyTokenName[yytos->major]);
  }
#endif
  yy_destructor(pParser, yytos->major, &yytos->minor);
}

/* 
** Deallocate and destroy a parser.  Destructors are called for
** all stack elements before shutting the parser down.
**
** If the YYPARSEFREENEVERNULL macro exists (for example because it
** is defined in a %include section of the input grammar) then it is
** assumed that the input pointer is never NULL.
*/
void ParseFree(
  void *p,                    /* The parser to be deleted */
  void (*freeProc)(void*)     /* Function used to reclaim memory */
){
  yyParser *pParser = (yyParser*)p;
#ifndef YYPARSEFREENEVERNULL
  if( pParser==0 ) return;
#endif
  while( pParser->yytos>pParser->yystack ) yy_pop_parser_stack(pParser);
#if YYSTACKDEPTH<=0
  if( pParser->yystack!=&pParser->yystk0 ) free(pParser->yystack);
#endif
  (*freeProc)((void*)pParser);
}

/*
** Return the peak depth of the stack for a parser.
*/
#ifdef YYTRACKMAXSTACKDEPTH
int ParseStackPeak(void *p){
  yyParser *pParser = (yyParser*)p;
  return pParser->yyhwm;
}
#endif

/*
** Find the appropriate action for a parser given the terminal
** look-ahead token iLookAhead.
*/
static unsigned int yy_find_shift_action(
  yyParser *pParser,        /* The parser */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
  int stateno = pParser->yytos->stateno;
 
  if( stateno>=YY_MIN_REDUCE ) return stateno;
  assert( stateno <= YY_SHIFT_COUNT );
  do{
    i = yy_shift_ofst[stateno];
    if( i==YY_SHIFT_USE_DFLT ) return yy_default[stateno];
    assert( iLookAhead!=YYNOCODE );
    i += iLookAhead;
    if( i<0 || i>=YY_ACTTAB_COUNT || yy_lookahead[i]!=iLookAhead ){
      if( iLookAhead>0 ){
#ifdef YYFALLBACK
        YYCODETYPE iFallback;            /* Fallback token */
        if( iLookAhead<sizeof(yyFallback)/sizeof(yyFallback[0])
               && (iFallback = yyFallback[iLookAhead])!=0 ){
#ifndef NDEBUG
          if( yyTraceFILE ){
            fprintf(yyTraceFILE, "%sFALLBACK %s => %s\n",
               yyTracePrompt, yyTokenName[iLookAhead], yyTokenName[iFallback]);
          }
#endif
          assert( yyFallback[iFallback]==0 ); /* Fallback loop must terminate */
          iLookAhead = iFallback;
          continue;
        }
#endif
#ifdef YYWILDCARD
        {
          int j = i - iLookAhead + YYWILDCARD;
          if( 
#if YY_SHIFT_MIN+YYWILDCARD<0
            j>=0 &&
#endif
#if YY_SHIFT_MAX+YYWILDCARD>=YY_ACTTAB_COUNT
            j<YY_ACTTAB_COUNT &&
#endif
            yy_lookahead[j]==YYWILDCARD
          ){
#ifndef NDEBUG
            if( yyTraceFILE ){
              fprintf(yyTraceFILE, "%sWILDCARD %s => %s\n",
                 yyTracePrompt, yyTokenName[iLookAhead],
                 yyTokenName[YYWILDCARD]);
            }
#endif /* NDEBUG */
            return yy_action[j];
          }
        }
#endif /* YYWILDCARD */
      }
      return yy_default[stateno];
    }else{
      return yy_action[i];
    }
  }while(1);
}

/*
** Find the appropriate action for a parser given the non-terminal
** look-ahead token iLookAhead.
*/
static int yy_find_reduce_action(
  int stateno,              /* Current state number */
  YYCODETYPE iLookAhead     /* The look-ahead token */
){
  int i;
#ifdef YYERRORSYMBOL
  if( stateno>YY_REDUCE_COUNT ){
    return yy_default[stateno];
  }
#else
  assert( stateno<=YY_REDUCE_COUNT );
#endif
  i = yy_reduce_ofst[stateno];
  assert( i!=YY_REDUCE_USE_DFLT );
  assert( iLookAhead!=YYNOCODE );
  i += iLookAhead;
#ifdef YYERRORSYMBOL
  if( i<0 || i>=YY_ACTTAB_COUNT || yy_lookahead[i]!=iLookAhead ){
    return yy_default[stateno];
  }
#else
  assert( i>=0 && i<YY_ACTTAB_COUNT );
  assert( yy_lookahead[i]==iLookAhead );
#endif
  return yy_action[i];
}

/*
** The following routine is called if the stack overflows.
*/
static void yyStackOverflow(yyParser *yypParser){
   ParseARG_FETCH;
   yypParser->yytos--;
#ifndef NDEBUG
   if( yyTraceFILE ){
     fprintf(yyTraceFILE,"%sStack Overflow!\n",yyTracePrompt);
   }
#endif
   while( yypParser->yytos>yypParser->yystack ) yy_pop_parser_stack(yypParser);
   /* Here code is inserted which will execute if the parser
   ** stack every overflows */
/******** Begin %stack_overflow code ******************************************/
/******** End %stack_overflow code ********************************************/
   ParseARG_STORE; /* Suppress warning about unused %extra_argument var */
}

/*
** Print tracing information for a SHIFT action
*/
#ifndef NDEBUG
static void yyTraceShift(yyParser *yypParser, int yyNewState){
  if( yyTraceFILE ){
    if( yyNewState<YYNSTATE ){
      fprintf(yyTraceFILE,"%sShift '%s', go to state %d\n",
         yyTracePrompt,yyTokenName[yypParser->yytos->major],
         yyNewState);
    }else{
      fprintf(yyTraceFILE,"%sShift '%s'\n",
         yyTracePrompt,yyTokenName[yypParser->yytos->major]);
    }
  }
}
#else
# define yyTraceShift(X,Y)
#endif

/*
** Perform a shift action.
*/
static void yy_shift(
  yyParser *yypParser,          /* The parser to be shifted */
  int yyNewState,               /* The new state to shift in */
  int yyMajor,                  /* The major token to shift in */
  ParseTOKENTYPE yyMinor        /* The minor token to shift in */
){
  yyStackEntry *yytos;
  yypParser->yytos++;
#ifdef YYTRACKMAXSTACKDEPTH
  if( (int)(yypParser->yytos - yypParser->yystack)>yypParser->yyhwm ){
    yypParser->yyhwm++;
    assert( yypParser->yyhwm == (int)(yypParser->yytos - yypParser->yystack) );
  }
#endif
#if YYSTACKDEPTH>0 
  if( yypParser->yytos>=&yypParser->yystack[YYSTACKDEPTH] ){
    yyStackOverflow(yypParser);
    return;
  }
#else
  if( yypParser->yytos>=&yypParser->yystack[yypParser->yystksz] ){
    if( yyGrowStack(yypParser) ){
      yyStackOverflow(yypParser);
      return;
    }
  }
#endif
  if( yyNewState > YY_MAX_SHIFT ){
    yyNewState += YY_MIN_REDUCE - YY_MIN_SHIFTREDUCE;
  }
  yytos = yypParser->yytos;
  yytos->stateno = (YYACTIONTYPE)yyNewState;
  yytos->major = (YYCODETYPE)yyMajor;
  yytos->minor.yy0 = yyMinor;
  yyTraceShift(yypParser, yyNewState);
}

/* The following table contains information about every rule that
** is used during the reduce.
*/
static const struct {
  YYCODETYPE lhs;         /* Symbol on the left-hand side of the rule */
  unsigned char nrhs;     /* Number of right-hand side symbols in the rule */
} yyRuleInfo[] = {
  { 60, 1 },
  { 61, 0 },
  { 61, 2 },
  { 62, 3 },
  { 63, 3 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 2 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 2 },
  { 63, 2 },
  { 63, 2 },
  { 63, 2 },
  { 63, 2 },
  { 63, 2 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 3 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 1 },
  { 63, 3 },
  { 63, 6 },
  { 63, 2 },
  { 63, 2 },
  { 63, 2 },
  { 63, 3 },
  { 63, 2 },
  { 62, 6 },
  { 62, 4 },
  { 63, 1 },
  { 64, 2 },
  { 64, 3 },
  { 65, 1 },
  { 65, 3 },
  { 62, 8 },
  { 62, 2 },
  { 66, 4 },
  { 67, 2 },
  { 67, 4 },
  { 67, 2 },
};

static void yy_accept(yyParser*);  /* Forward Declaration */

/*
** Perform a reduce action and the shift that must immediately
** follow the reduce.
*/
static void yy_reduce(
  yyParser *yypParser,         /* The parser */
  unsigned int yyruleno        /* Number of the rule by which to reduce */
){
  int yygoto;                     /* The next state */
  int yyact;                      /* The next action */
  yyStackEntry *yymsp;            /* The top of the parser's stack */
  int yysize;                     /* Amount to pop the stack */
  ParseARG_FETCH;
  yymsp = yypParser->yytos;
#ifndef NDEBUG
  if( yyTraceFILE && yyruleno<(int)(sizeof(yyRuleName)/sizeof(yyRuleName[0])) ){
    yysize = yyRuleInfo[yyruleno].nrhs;
    fprintf(yyTraceFILE, "%sReduce [%s], go to state %d.\n", yyTracePrompt,
      yyRuleName[yyruleno], yymsp[-yysize].stateno);
  }
#endif /* NDEBUG */

  /* Check that the stack is large enough to grow by a single entry
  ** if the RHS of the rule is empty.  This ensures that there is room
  ** enough on the stack to push the LHS value */
  if( yyRuleInfo[yyruleno].nrhs==0 ){
#ifdef YYTRACKMAXSTACKDEPTH
    if( (int)(yypParser->yytos - yypParser->yystack)>yypParser->yyhwm ){
      yypParser->yyhwm++;
      assert( yypParser->yyhwm == (int)(yypParser->yytos - yypParser->yystack));
    }
#endif
#if YYSTACKDEPTH>0 
    if( yypParser->yytos>=&yypParser->yystack[YYSTACKDEPTH-1] ){
      yyStackOverflow(yypParser);
      return;
    }
#else
    if( yypParser->yytos>=&yypParser->yystack[yypParser->yystksz-1] ){
      if( yyGrowStack(yypParser) ){
        yyStackOverflow(yypParser);
        return;
      }
      yymsp = yypParser->yytos;
    }
#endif
  }

  switch( yyruleno ){
  /* Beginning here are the reduction cases.  A typical example
  ** follows:
  **   case 0:
  **  #line <lineno> <grammarfile>
  **     { ... }           // User supplied code
  **  #line <lineno> <thisfile>
  **     break;
  */
/********** Begin reduce actions **********************************************/
        YYMINORTYPE yylhsminor;
      case 0: /* code ::= statementblock */
#line 246 "grammar.y"
{
	printf (cJSON_Print(yymsp[0].minor.yy0)); 
}
#line 1005 "grammar.c"
        break;
      case 1: /* statementblock ::= */
#line 258 "grammar.y"
{
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "STATEMENTBLOCK");
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToObject(res, "statements", arg); 
	yymsp[1].minor.yy0 = res;
}
#line 1016 "grammar.c"
        break;
      case 2: /* statementblock ::= statementblock statement */
#line 268 "grammar.y"
{
	cJSON_AddItemToArray(cJSON_GetObjectItem ( yymsp[-1].minor.yy0, "statements"), yymsp[0].minor.yy0);
	yylhsminor.yy0 = yymsp[-1].minor.yy0;
}
#line 1024 "grammar.c"
  yymsp[-1].minor.yy0 = yylhsminor.yy0;
        break;
      case 3: /* statement ::= WRITE ex SEMICOLON */
#line 280 "grammar.y"
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON_AddStringToObject(res, "type", "WRITE"); 
	cJSON_AddItemToObject(res, "arg", yymsp[-1].minor.yy0); 
	yymsp[-2].minor.yy0 = res; 
}
#line 1035 "grammar.c"
        break;
      case 4: /* ex ::= LPAR ex RPAR */
#line 291 "grammar.y"
{ 
	yymsp[-2].minor.yy0 = yymsp[-1].minor.yy0; 
}
#line 1042 "grammar.c"
        break;
      case 5: /* ex ::= NUMTOKEN */
#line 297 "grammar.y"
{ 
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "NUMTOKEN");
	cJSON_AddStringToObject(res, "value", getValue(yymsp[0].minor.yy0)); 
	yylhsminor.yy0 = res; 
}
#line 1052 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 6: /* ex ::= STRTOKEN */
#line 306 "grammar.y"
{ 
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "STRTOKEN"); 
	cJSON_AddStringToObject(res, "value", getValue(yymsp[0].minor.yy0)); 
	yylhsminor.yy0 = res; 
}
#line 1063 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 7: /* ex ::= TIMESTAMP */
#line 314 "grammar.y"
{
	cJSON *res = cJSON_CreateObject();
	cJSON_AddStringToObject(res, "type", "TIMESTAMP");
	cJSON_AddStringToObject(res, "value", getValue(yymsp[0].minor.yy0));
	yylhsminor.yy0 = res;
}
#line 1074 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 8: /* ex ::= IDENTIFIER */
#line 323 "grammar.y"
{ 
	cJSON *res = cJSON_CreateObject(); 
	cJSON_AddStringToObject(res, "type", "VARIABLE"); 
	cJSON_AddStringToObject(res, "name", getValue(yymsp[0].minor.yy0)); 
	cJSON_AddStringToObject(res, "line", getLine(yymsp[0].minor.yy0)); 
	yylhsminor.yy0 = res; 
}
#line 1086 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 9: /* ex ::= TRUE */
#line 332 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "TRUE");
    cJSON_AddStringToObject(res, "value", "TRUE");
    yylhsminor.yy0 = res;
}
#line 1097 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 10: /* ex ::= FALSE */
#line 340 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "FALSE");
    cJSON_AddStringToObject(res, "value", "FALSE");
    yylhsminor.yy0 = res;
}
#line 1108 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 11: /* ex ::= NULLTOKEN */
#line 348 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NULL");
    cJSON_AddStringToObject(res, "value", "NULL");
    yylhsminor.yy0 = res;
}
#line 1119 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 12: /* ex ::= NOW */
#line 356 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NOW");
    cJSON_AddStringToObject(res, "value", "NOW");
    yylhsminor.yy0 = res;
}
#line 1130 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 13: /* ex ::= ex PLUS ex */
#line 368 "grammar.y"
{yylhsminor.yy0 = binary ("PLUS", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1136 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 14: /* ex ::= ex MINUS ex */
#line 371 "grammar.y"
{yylhsminor.yy0 = binary ("MINUS", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1142 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 15: /* ex ::= ex TIMES ex */
#line 374 "grammar.y"
{yylhsminor.yy0 = binary ("TIMES", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1148 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 16: /* ex ::= ex DIVIDE ex */
#line 377 "grammar.y"
{yylhsminor.yy0 = binary ("DIVIDE", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1154 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 17: /* ex ::= ex POWER ex */
#line 380 "grammar.y"
{yylhsminor.yy0 = binary ("POWER", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1160 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 18: /* ex ::= NOT ex */
#line 383 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("NOT", yymsp[0].minor.yy0); }
#line 1166 "grammar.c"
        break;
      case 19: /* ex ::= ex AND ex */
#line 386 "grammar.y"
{yylhsminor.yy0 = binary ("AND", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1171 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 20: /* ex ::= ex OR ex */
#line 389 "grammar.y"
{yylhsminor.yy0 = binary ("OR", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1177 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 21: /* ex ::= ex SEQTO ex */
#line 392 "grammar.y"
{yylhsminor.yy0 = binary ("SEQTO", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1183 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 22: /* ex ::= MAXOP ex */
#line 395 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("MAXOP", yymsp[0].minor.yy0); }
#line 1189 "grammar.c"
        break;
      case 23: /* ex ::= MINOP ex */
#line 398 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("MINOP", yymsp[0].minor.yy0); }
#line 1194 "grammar.c"
        break;
      case 24: /* ex ::= FIRSTOP ex */
#line 401 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("FIRSTOP", yymsp[0].minor.yy0); }
#line 1199 "grammar.c"
        break;
      case 25: /* ex ::= LASTOP ex */
#line 404 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("LASTOP", yymsp[0].minor.yy0); }
#line 1204 "grammar.c"
        break;
      case 26: /* ex ::= SUMOP ex */
#line 407 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("SUMOP", yymsp[0].minor.yy0); }
#line 1209 "grammar.c"
        break;
      case 27: /* ex ::= COUNTOP ex */
#line 410 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("COUNTOP", yymsp[0].minor.yy0); }
#line 1214 "grammar.c"
        break;
      case 28: /* ex ::= ex EQUAL ex */
#line 413 "grammar.y"
{yylhsminor.yy0 = binary ("EQUAL", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1219 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 29: /* ex ::= ex UNEQUAL ex */
#line 416 "grammar.y"
{yylhsminor.yy0 = binary ("UNEQUAL", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1225 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 30: /* ex ::= ex LT ex */
#line 419 "grammar.y"
{yylhsminor.yy0 = binary ("LT", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1231 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 31: /* ex ::= ex LTEQ ex */
#line 422 "grammar.y"
{yylhsminor.yy0 = binary ("LTEQ", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1237 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 32: /* ex ::= ex GT ex */
#line 425 "grammar.y"
{yylhsminor.yy0 = binary ("GT", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1243 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 33: /* ex ::= ex GTEQ ex */
#line 428 "grammar.y"
{yylhsminor.yy0 = binary ("GTEQ", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1249 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 34: /* ex ::= NUMBER */
#line 431 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "NUMBER");
    cJSON_AddStringToObject(res, "value", "NUMBER");
    yylhsminor.yy0 = res;
}
#line 1260 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 35: /* ex ::= TEXT */
#line 439 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "TEXT");
    cJSON_AddStringToObject(res, "value", "TEXT");
    yylhsminor.yy0 = res;
}
#line 1271 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 36: /* ex ::= LIST */
#line 447 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "LIST");
    cJSON_AddStringToObject(res, "value", "LIST");
    yylhsminor.yy0 = res;
}
#line 1282 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 37: /* ex ::= BOOLEAN */
#line 455 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "BOOLEAN");
    cJSON_AddStringToObject(res, "value", "BOOLEAN");
    yylhsminor.yy0 = res;
}
#line 1293 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 38: /* ex ::= ex IS ex */
#line 463 "grammar.y"
{yylhsminor.yy0 = binary ("IS", yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1299 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 39: /* ex ::= ex IS WITHIN ex TO ex */
#line 466 "grammar.y"
{yylhsminor.yy0 = ternary ("WITHIN", yymsp[-5].minor.yy0, yymsp[-2].minor.yy0, yymsp[0].minor.yy0); }
#line 1305 "grammar.c"
  yymsp[-5].minor.yy0 = yylhsminor.yy0;
        break;
      case 40: /* ex ::= EARLIEST ex */
#line 469 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("EARLIEST", yymsp[0].minor.yy0); }
#line 1311 "grammar.c"
        break;
      case 41: /* ex ::= MINUS ex */
#line 472 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("UMINUS", yymsp[0].minor.yy0); }
#line 1316 "grammar.c"
        break;
      case 42: /* ex ::= READ ex */
#line 475 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("READ", yymsp[0].minor.yy0); }
#line 1321 "grammar.c"
        break;
      case 43: /* ex ::= TIME OF ex */
#line 480 "grammar.y"
{yymsp[-2].minor.yy0 = unary ("TIME", yymsp[0].minor.yy0); }
#line 1326 "grammar.c"
        break;
      case 44: /* ex ::= TIME ex */
#line 483 "grammar.y"
{yymsp[-1].minor.yy0 = unary ("TIME", yymsp[0].minor.yy0); }
#line 1331 "grammar.c"
        break;
      case 45: /* statement ::= TIME OF IDENTIFIER ASSIGN ex SEMICOLON */
#line 486 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "SETTIME");
    cJSON_AddStringToObject(res, "varname", getValue(yymsp[-3].minor.yy0));
    cJSON_AddItemToObject(res, "arg", yymsp[-1].minor.yy0); yymsp[-5].minor.yy0 = res;
}
#line 1341 "grammar.c"
        break;
      case 46: /* statement ::= IDENTIFIER ASSIGN ex SEMICOLON */
#line 495 "grammar.y"
{ 
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "ASSIGNMENT");
    cJSON_AddStringToObject(res, "varname", getValue(yymsp[-3].minor.yy0));
    cJSON_AddItemToObject(res, "arg", yymsp[-1].minor.yy0); yylhsminor.yy0 = res;
}
#line 1351 "grammar.c"
  yymsp[-3].minor.yy0 = yylhsminor.yy0;
        break;
      case 47: /* ex ::= jsonarray */
#line 504 "grammar.y"
{yylhsminor.yy0 = yymsp[0].minor.yy0;}
#line 1357 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 48: /* jsonarray ::= LBRACKET RBRACKET */
#line 507 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "EMPTYLIST");
    yymsp[-1].minor.yy0 = res;
}
#line 1367 "grammar.c"
        break;
      case 49: /* jsonarray ::= LBRACKET exlist RBRACKET */
#line 515 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "LISTTOKEN");
    cJSON_AddItemToObject(res, "args", yymsp[-1].minor.yy0);
    yymsp[-2].minor.yy0 = res;
}
#line 1377 "grammar.c"
        break;
      case 50: /* exlist ::= ex */
#line 524 "grammar.y"
{
    cJSON *arg = cJSON_CreateArray();
    cJSON_AddItemToArray(arg, yymsp[0].minor.yy0); yylhsminor.yy0 = arg;
}
#line 1385 "grammar.c"
  yymsp[0].minor.yy0 = yylhsminor.yy0;
        break;
      case 51: /* exlist ::= exlist COMMA ex */
#line 531 "grammar.y"
{
    cJSON_AddItemToArray(yymsp[-2].minor.yy0,yymsp[0].minor.yy0);
    yylhsminor.yy0 = yymsp[-2].minor.yy0;
}
#line 1394 "grammar.c"
  yymsp[-2].minor.yy0 = yylhsminor.yy0;
        break;
      case 52: /* statement ::= FOR IDENTIFIER IN ex DO statement ENDDO SEMICOLON */
#line 541 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "FOR");
    cJSON_AddStringToObject(res, "varname", getValue(yymsp[-6].minor.yy0));
    cJSON_AddItemToObject(res, "expression", yymsp[-4].minor.yy0);
    cJSON_AddItemToObject(res, "statements", yymsp[-2].minor.yy0);
    yymsp[-7].minor.yy0 = res;
}
#line 1407 "grammar.c"
        break;
      case 53: /* statement ::= IF if_then_else */
      case 57: /* elseif ::= ELSEIF if_then_else */ yytestcase(yyruleno==57);
#line 555 "grammar.y"
{yymsp[-1].minor.yy0 = yymsp[0].minor.yy0;}
#line 1413 "grammar.c"
        break;
      case 54: /* if_then_else ::= ex THEN statementblock elseif */
#line 558 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "IF");
    cJSON_AddItemToObject(res, "condition", yymsp[-3].minor.yy0);
    cJSON_AddItemToObject(res, "thenbranch", (yymsp[-1].minor.yy0));
    cJSON_AddItemToObject(res, "elsebranch", (yymsp[0].minor.yy0));
    yylhsminor.yy0 = res;
}
#line 1425 "grammar.c"
  yymsp[-3].minor.yy0 = yylhsminor.yy0;
        break;
      case 55: /* elseif ::= ENDIF SEMICOLON */
#line 568 "grammar.y"
{
    cJSON *res = cJSON_CreateObject();
    cJSON_AddStringToObject(res, "type", "STATEMENTBLOCK");
    cJSON *arg = cJSON_CreateArray();
    cJSON_AddItemToObject(res, "statements", arg);
    yymsp[-1].minor.yy0 = res;
}
#line 1437 "grammar.c"
        break;
      case 56: /* elseif ::= ELSE statementblock ENDIF SEMICOLON */
#line 577 "grammar.y"
{yymsp[-3].minor.yy0 = yymsp[-2].minor.yy0;}
#line 1442 "grammar.c"
        break;
      default:
        break;
/********** End reduce actions ************************************************/
  };
  assert( yyruleno<sizeof(yyRuleInfo)/sizeof(yyRuleInfo[0]) );
  yygoto = yyRuleInfo[yyruleno].lhs;
  yysize = yyRuleInfo[yyruleno].nrhs;
  yyact = yy_find_reduce_action(yymsp[-yysize].stateno,(YYCODETYPE)yygoto);
  if( yyact <= YY_MAX_SHIFTREDUCE ){
    if( yyact>YY_MAX_SHIFT ){
      yyact += YY_MIN_REDUCE - YY_MIN_SHIFTREDUCE;
    }
    yymsp -= yysize-1;
    yypParser->yytos = yymsp;
    yymsp->stateno = (YYACTIONTYPE)yyact;
    yymsp->major = (YYCODETYPE)yygoto;
    yyTraceShift(yypParser, yyact);
  }else{
    assert( yyact == YY_ACCEPT_ACTION );
    yypParser->yytos -= yysize;
    yy_accept(yypParser);
  }
}

/*
** The following code executes when the parse fails
*/
#ifndef YYNOERRORRECOVERY
static void yy_parse_failed(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sFail!\n",yyTracePrompt);
  }
#endif
  while( yypParser->yytos>yypParser->yystack ) yy_pop_parser_stack(yypParser);
  /* Here code is inserted which will be executed whenever the
  ** parser fails */
/************ Begin %parse_failure code ***************************************/
/************ End %parse_failure code *****************************************/
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}
#endif /* YYNOERRORRECOVERY */

/*
** The following code executes when a syntax error first occurs.
*/
static void yy_syntax_error(
  yyParser *yypParser,           /* The parser */
  int yymajor,                   /* The major type of the error token */
  ParseTOKENTYPE yyminor         /* The minor type of the error token */
){
  ParseARG_FETCH;
#define TOKEN yyminor
/************ Begin %syntax_error code ****************************************/
#line 213 "grammar.y"

  printf ("{\"error\" : true, \"message\": \"Syntax Error: MyCompiler reports unexpected token \\\"%s\\\" of type \\\"%s\\\" in line %s\"}\n", curtoken, curtype, linenumber);
  exit(0);
#line 1505 "grammar.c"
/************ End %syntax_error code ******************************************/
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/*
** The following is executed when the parser accepts
*/
static void yy_accept(
  yyParser *yypParser           /* The parser */
){
  ParseARG_FETCH;
#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sAccept!\n",yyTracePrompt);
  }
#endif
#ifndef YYNOERRORRECOVERY
  yypParser->yyerrcnt = -1;
#endif
  assert( yypParser->yytos==yypParser->yystack );
  /* Here code is inserted which will be executed whenever the
  ** parser accepts */
/*********** Begin %parse_accept code *****************************************/
/*********** End %parse_accept code *******************************************/
  ParseARG_STORE; /* Suppress warning about unused %extra_argument variable */
}

/* The main parser program.
** The first argument is a pointer to a structure obtained from
** "ParseAlloc" which describes the current state of the parser.
** The second argument is the major token number.  The third is
** the minor token.  The fourth optional argument is whatever the
** user wants (and specified in the grammar) and is available for
** use by the action routines.
**
** Inputs:
** <ul>
** <li> A pointer to the parser (an opaque structure.)
** <li> The major token number.
** <li> The minor token number.
** <li> An option argument of a grammar-specified type.
** </ul>
**
** Outputs:
** None.
*/
void Parse(
  void *yyp,                   /* The parser */
  int yymajor,                 /* The major token code number */
  ParseTOKENTYPE yyminor       /* The value for the token */
  ParseARG_PDECL               /* Optional %extra_argument parameter */
){
  YYMINORTYPE yyminorunion;
  unsigned int yyact;   /* The parser action. */
#if !defined(YYERRORSYMBOL) && !defined(YYNOERRORRECOVERY)
  int yyendofinput;     /* True if we are at the end of input */
#endif
#ifdef YYERRORSYMBOL
  int yyerrorhit = 0;   /* True if yymajor has invoked an error */
#endif
  yyParser *yypParser;  /* The parser */

  yypParser = (yyParser*)yyp;
  assert( yypParser->yytos!=0 );
#if !defined(YYERRORSYMBOL) && !defined(YYNOERRORRECOVERY)
  yyendofinput = (yymajor==0);
#endif
  ParseARG_STORE;

#ifndef NDEBUG
  if( yyTraceFILE ){
    fprintf(yyTraceFILE,"%sInput '%s'\n",yyTracePrompt,yyTokenName[yymajor]);
  }
#endif

  do{
    yyact = yy_find_shift_action(yypParser,(YYCODETYPE)yymajor);
    if( yyact <= YY_MAX_SHIFTREDUCE ){
      yy_shift(yypParser,yyact,yymajor,yyminor);
#ifndef YYNOERRORRECOVERY
      yypParser->yyerrcnt--;
#endif
      yymajor = YYNOCODE;
    }else if( yyact <= YY_MAX_REDUCE ){
      yy_reduce(yypParser,yyact-YY_MIN_REDUCE);
    }else{
      assert( yyact == YY_ERROR_ACTION );
      yyminorunion.yy0 = yyminor;
#ifdef YYERRORSYMBOL
      int yymx;
#endif
#ifndef NDEBUG
      if( yyTraceFILE ){
        fprintf(yyTraceFILE,"%sSyntax Error!\n",yyTracePrompt);
      }
#endif
#ifdef YYERRORSYMBOL
      /* A syntax error has occurred.
      ** The response to an error depends upon whether or not the
      ** grammar defines an error token "ERROR".  
      **
      ** This is what we do if the grammar does define ERROR:
      **
      **  * Call the %syntax_error function.
      **
      **  * Begin popping the stack until we enter a state where
      **    it is legal to shift the error symbol, then shift
      **    the error symbol.
      **
      **  * Set the error count to three.
      **
      **  * Begin accepting and shifting new tokens.  No new error
      **    processing will occur until three tokens have been
      **    shifted successfully.
      **
      */
      if( yypParser->yyerrcnt<0 ){
        yy_syntax_error(yypParser,yymajor,yyminor);
      }
      yymx = yypParser->yytos->major;
      if( yymx==YYERRORSYMBOL || yyerrorhit ){
#ifndef NDEBUG
        if( yyTraceFILE ){
          fprintf(yyTraceFILE,"%sDiscard input token %s\n",
             yyTracePrompt,yyTokenName[yymajor]);
        }
#endif
        yy_destructor(yypParser, (YYCODETYPE)yymajor, &yyminorunion);
        yymajor = YYNOCODE;
      }else{
        while( yypParser->yytos >= &yypParser->yystack
            && yymx != YYERRORSYMBOL
            && (yyact = yy_find_reduce_action(
                        yypParser->yytos->stateno,
                        YYERRORSYMBOL)) >= YY_MIN_REDUCE
        ){
          yy_pop_parser_stack(yypParser);
        }
        if( yypParser->yytos < yypParser->yystack || yymajor==0 ){
          yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
          yy_parse_failed(yypParser);
#ifndef YYNOERRORRECOVERY
          yypParser->yyerrcnt = -1;
#endif
          yymajor = YYNOCODE;
        }else if( yymx!=YYERRORSYMBOL ){
          yy_shift(yypParser,yyact,YYERRORSYMBOL,yyminor);
        }
      }
      yypParser->yyerrcnt = 3;
      yyerrorhit = 1;
#elif defined(YYNOERRORRECOVERY)
      /* If the YYNOERRORRECOVERY macro is defined, then do not attempt to
      ** do any kind of error recovery.  Instead, simply invoke the syntax
      ** error routine and continue going as if nothing had happened.
      **
      ** Applications can set this macro (for example inside %include) if
      ** they intend to abandon the parse upon the first syntax error seen.
      */
      yy_syntax_error(yypParser,yymajor, yyminor);
      yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
      yymajor = YYNOCODE;
      
#else  /* YYERRORSYMBOL is not defined */
      /* This is what we do if the grammar does not define ERROR:
      **
      **  * Report an error message, and throw away the input token.
      **
      **  * If the input token is $, then fail the parse.
      **
      ** As before, subsequent error messages are suppressed until
      ** three input tokens have been successfully shifted.
      */
      if( yypParser->yyerrcnt<=0 ){
        yy_syntax_error(yypParser,yymajor, yyminor);
      }
      yypParser->yyerrcnt = 3;
      yy_destructor(yypParser,(YYCODETYPE)yymajor,&yyminorunion);
      if( yyendofinput ){
        yy_parse_failed(yypParser);
#ifndef YYNOERRORRECOVERY
        yypParser->yyerrcnt = -1;
#endif
      }
      yymajor = YYNOCODE;
#endif
    }
  }while( yymajor!=YYNOCODE && yypParser->yytos>yypParser->yystack );
#ifndef NDEBUG
  if( yyTraceFILE ){
    yyStackEntry *i;
    char cDiv = '[';
    fprintf(yyTraceFILE,"%sReturn. Stack=",yyTracePrompt);
    for(i=&yypParser->yystack[1]; i<=yypParser->yytos; i++){
      fprintf(yyTraceFILE,"%c%s", cDiv, yyTokenName[i->major]);
      cDiv = ' ';
    }
    fprintf(yyTraceFILE,"]\n");
  }
#endif
  return;
}
#line 25 "grammar.y"


using namespace std;
typedef struct {char *value; int line;} token;

token* create_token (char *value, int line) {
	token *t = (token*) malloc (sizeof (token));
	t->value = strdup (value);
	t->line = line;
	return t;
}

const char * getValue (cJSON* token) {
	return cJSON_GetObjectItem (token, "value")->valuestring;
}


const char * getLine (cJSON* token) {
	return cJSON_GetObjectItem (token, "line")->valuestring;
}


int main(int argc, char* argv[]) {
	char *result;
	std::string line;
	std::string input = "";
	while (std::getline(std::cin, line)) {
      input += line + "\n";
    }
	if (input == "") {
		cout << "Empty input";
		exit(0);
	}
	
	cJSON *root = cJSON_Parse(input.c_str());
	
	if (!root) {
		cout << "JSON invalid\n";
		exit(0);
	}
	
	void* pParser = ParseAlloc (malloc);
	int num = cJSON_GetArraySize (root);
	
	for (int i = 0; i < num; i++ ) {
	
		// Knoten im Token-Stream auslesen
		cJSON *node = cJSON_GetArrayItem(root,i);
		
		char *line = cJSON_GetArrayItem(node,0)->valuestring;
		char *type = cJSON_GetArrayItem(node,1)->valuestring;
		char *value = cJSON_GetArrayItem(node,2)->valuestring;
		
		cJSON *tok = cJSON_CreateObject();
		cJSON_AddStringToObject(tok, "value", value);
		cJSON_AddStringToObject(tok, "line", line);

		linenumber = line;
		curtoken = value;
		curtype = type;
		// THE und Kommentare werden ueberlesen
		if (strcmp(type, "THE") == 0) continue;
		if (strcmp(type, "COMMENT") == 0) continue;
		if (strcmp(type, "MCOMMENT") == 0) continue;
		
		int tokenid = get_token_id (type);
		Parse (pParser, tokenid, tok);
		
	}
	Parse (pParser, 0, 0);
    ParseFree(pParser, free );
}




/////////////////////// 
/////////////////////// 
// TOKENS
///////////////////////
/////////////////////// 

int get_token_id (char *token) {
    if (strcmp(token, "AND") == 0) return AND;
    if (strcmp(token, "ASSIGN") == 0) return ASSIGN;
    if (strcmp(token, "BOOLEAN") == 0) return BOOLEAN;
	if (strcmp(token, "COMMA") == 0) return COMMA;
	if (strcmp(token, "COUNTOP") == 0) return COUNTOP;
	if (strcmp(token, "DIVIDE") == 0) return DIVIDE;
	if (strcmp(token, "DO") == 0) return DO;
	if (strcmp(token, "EARLIEST") == 0) return EARLIEST;
	if (strcmp(token, "ELSE") == 0) return ELSE;
	if (strcmp(token, "ELSEIF") == 0) return ELSEIF;
	if (strcmp(token, "ENDDO") == 0) return ENDDO;
	if (strcmp(token, "ENDIF") == 0) return ENDIF;
	if (strcmp(token, "EQUAL") == 0) return EQUAL;
	if (strcmp(token, "FIRSTOP") == 0) return FIRSTOP;
	if (strcmp(token, "GT") == 0) return GT;
	if (strcmp(token, "GTEQ") == 0) return GTEQ;
	if (strcmp(token, "IDENTIFIER") == 0) return IDENTIFIER;
	if (strcmp(token, "IF") == 0) return IF;
	if (strcmp(token, "IN") == 0) return IN;
	if (strcmp(token, "IS") == 0) return IS;
	if (strcmp(token, "LPAR") == 0) return LPAR;
	if (strcmp(token, "LT") == 0) return LT;
	if (strcmp(token, "LTEQ") == 0) return LTEQ;
	if (strcmp(token, "RPAR") == 0) return RPAR;
	if (strcmp(token, "MINUS") == 0) return MINUS;
	if (strcmp(token, "NUMTOKEN") == 0) return NUMTOKEN;
	if (strcmp(token, "TRUE") == 0) return TRUE;
	if (strcmp(token, "FALSE") == 0) return FALSE;
	if (strcmp(token, "FOR") == 0) return FOR;
	if (strcmp(token, "LASTOP") == 0) return LASTOP;
	if (strcmp(token, "LBRACKET") == 0) return LBRACKET;
	if (strcmp(token, "LIST") == 0) return LIST;
	if (strcmp(token, "MAXOP") == 0) return MAXOP;
	if (strcmp(token, "MINOP") == 0) return MINOP;
	if (strcmp(token, "NOT") == 0) return NOT;
	if (strcmp(token, "NOW") == 0) return NOW;
	if (strcmp(token, "NULL") == 0) return NULLTOKEN;
	if (strcmp(token, "NUMBER") == 0) return NUMBER;
	if (strcmp(token, "OF") == 0) return OF;
	if (strcmp(token, "OR") == 0) return OR;
	if (strcmp(token, "PLUS") == 0) return PLUS;
	if (strcmp(token, "POWER") == 0) return POWER;
    if (strcmp(token, "RBRACKET") == 0) return RBRACKET;
    if (strcmp(token, "READ") == 0) return READ;
	if (strcmp(token, "SEMICOLON") == 0) return SEMICOLON;
	if (strcmp(token, "SEQTO") == 0) return SEQTO;
	if (strcmp(token, "STRTOKEN") == 0) return STRTOKEN;
	if (strcmp(token, "SUMOP") == 0) return SUMOP;
	if (strcmp(token, "TEXT") == 0) return TEXT;
	if (strcmp(token, "THEN") == 0) return THEN;
	if (strcmp(token, "TIME") == 0) return TIME;
    if (strcmp(token, "TIMES") == 0) return TIMES;
    if (strcmp(token, "TIMESTAMP") == 0) return TIMESTAMP;
    if (strcmp(token, "TO") == 0) return TO;
    if (strcmp(token, "UNEQUAL") == 0) return UNEQUAL;
	if (strcmp(token, "WRITE") == 0) return WRITE;
	if (strcmp(token, "WITHIN") == 0) return WITHIN;
	
	printf ("{\"error\" : true, \"message\": \"UNKNOWN TOKEN TYPE %s\"}\n", token);
	exit(0);
} 
  


cJSON* unary (char* fname, cJSON* a) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a);
	cJSON_AddStringToObject(res, "type", fname);
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
} 



cJSON* binary (char *fname, cJSON *a, cJSON *b) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a); 
	cJSON_AddItemToArray(arg, b);
	cJSON_AddStringToObject(res, "type", fname); 
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
}



cJSON* ternary (char *fname, cJSON *a, cJSON *b, cJSON *c) 
{
	cJSON *res = cJSON_CreateObject(); 
	cJSON *arg = cJSON_CreateArray();
	cJSON_AddItemToArray(arg, a); 
	cJSON_AddItemToArray(arg, b); 
	cJSON_AddItemToArray(arg, c);
	cJSON_AddStringToObject(res, "type", fname); 
	cJSON_AddItemToObject(res, "arg", arg);
	return res;
}



#line 1895 "grammar.c"

bmi := 41;

if bmi < 30 then
    write "Normalgewicht";
elseif bmi < 35 then
    write "Adipositas I";
elseif bmi < 40 then
    write "Adipositas II";
else
    write "Adipositas III";
endif;
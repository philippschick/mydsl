// Das sollte überall funzen

write "Hello World";


// NULL ist ein Type

write null;


// Grundrechenarten

write 1 + 2 - 5 * 7 / 9 ** 2;


// Jetzt mit Listen

write [1,1,1] + 2 - [5,5,5] * 7 / 9;


// Entspricht SEQTO in Arden

write 100 ... 200;


// Ein paar Aggregationsoperatoren

write maximum [20,30,40,100,50];

write minimum [20,30,40,100,50];

write first [20,30,40,100,50];

write last [20,30,40,100,50];

write sum [20,30,40,100,50];

write last 4711;

write count [20,30,40,100,50];


// Boolesche Operatoren

write true and not false;


// Vergleichsoperatoren

write 1 < 2;

write 2 <= 2;

write 2 = 2;

write 5 > 0;

write 5 is within 3 to 8;

write 5 is within 3 to [8,9];

write [1,2,null,"Hallo", now] is number;

write [1,2,"Hallo", now] is list;


// Variablenzuweisung

x := 1;

write x;


// TIME-Typ

write 2022-12-06T12:00:00;

write now;

write currenttime;


// Primary Times, interne Zeitstempel

x := 4711;

write time of x;

time of x := now;

write time of x;


// Erhalt von Zeitstempeln

y := 4712;

time of y := now;


z := x + y;

write z;

write time of z;


l1 := [x,y];

l2 := [x,y];

write time (l1 + l2);


write earliest l1;